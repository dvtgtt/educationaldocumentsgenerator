SET AssemblyName=EducationalProgramGenerator.DAL
SET StartUpDirectory=Project\EducationalProgramGenerator.DAL\bin\Release
SET ConnectionString=Data Source=WIN-6QRF800KKN1\SQLEXPRESS; Initial Catalog=OpenEducationDB; Persist Security Info=true; User ID=OpenEducation; Password=123456
SET ConnectionStringProvider=System.Data.SqlClient
SET ConfigFilePath=%CD%\Project\EducationalProgramGenerator.WEB\web.config
SET MigrateExe=Project\packages\EntityFramework.6.1.3\tools\migrate.exe

%MigrateExe% %AssemblyName%.dll /startUpDirectory:%StartUpDirectory% /startUpConfigurationFile:"%ConfigFilePath%" /connectionProviderName:"%ConnectionStringProvider%" /connectionString:"%ConnectionString%" /verbose
pause