﻿using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    public class StructureDepartmentService : IStructureDepartmentService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper;

        public StructureDepartmentService(IUnitOfWork uow)
        {
            Database = uow;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
            cfg => cfg.CreateMap<StructureDepartment, StructureDepartmentDTO>().PreserveReferences().ReverseMap().MaxDepth(500));
            mapper = config.CreateMapper();
        }

        public IEnumerable<StructureDepartmentDTO> GetAll()
        {
            return mapper.Map<IEnumerable<StructureDepartment>, IEnumerable<StructureDepartmentDTO>>(Database.CRUDRepository.Select<StructureDepartment>().Where(x => x.Children.Count == 0).ToList());
        }

        public StructureDepartmentDTO Get(int id)
        {
            return mapper.Map<StructureDepartment, StructureDepartmentDTO>(Database.CRUDRepository.Get<StructureDepartment>(id));
        }

        public void Update(StructureDepartmentDTO structureDepartmentDTO)
        {
            Database.CRUDRepository.Update(mapper.Map<StructureDepartmentDTO, StructureDepartment>(structureDepartmentDTO));
            Database.Save();
        }

        public void Delete(int id)
        {
            var structureDepartment = Database.CRUDRepository.Select<StructureDepartment>().Where(x => x.Id == id).FirstOrDefault();
            Database.CRUDRepository.Delete(structureDepartment);
            Database.Save();
        }

        public void Create(StructureDepartmentDTO structureDepartmentDTO)
        {
            Database.CRUDRepository.Create(mapper.Map<StructureDepartmentDTO, StructureDepartment>(structureDepartmentDTO));
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
