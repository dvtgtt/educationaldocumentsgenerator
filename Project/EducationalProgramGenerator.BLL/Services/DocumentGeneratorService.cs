﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using Newtonsoft.Json;
using TemplateEngine.Docx;

namespace EducationalProgramGenerator.BLL.Services
{
    public class DocumentGeneratorService : IDocumentGeneratorService
    {
        private IDisciplineService disciplineService;

        public DocumentGeneratorService(IDisciplineService disciplineService)
        {
            this.disciplineService = disciplineService;
        }

        public string CreateDocument(AcademicPlanItemDTO academicPlan, PlanItemDTO disciplinePlanItem, int structureDepartmentId)
        {
            var disciplineDto = (DisciplineDTO)disciplinePlanItem.Knowledges[0];
            var discipline = disciplineDto.Name;
            var filename = discipline.Replace(' ', '_') + ".docx";
            File.Delete(AppDomain.CurrentDomain.BaseDirectory + @"\Rpds\" + filename);
            File.Copy(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\template.docx", AppDomain.CurrentDomain.BaseDirectory + @"\Rpds\" + filename);

            var results = new TableContent(nameof(disciplineDto.Results));

            foreach (var result in disciplineDto.Results)
            {
                if (result.GetType() == typeof(CompetencyDTO))
                {
                    var r = (CompetencyDTO)result;
                    var zuv = disciplineDto.Results.FindAll(
                        res => res.GetType() == typeof(DisciplineLODTO) && ((DisciplineLODTO) res).Competency == r)
                        .Cast<DisciplineLODTO>()
                        .GroupBy(res => res.Type);
                    var zuvText = String.Join("\r\n", zuv.Select(pair => pair.Key + ": " + String.Join("; ", pair.Select(res => res.Description))).ToArray());
                    var content = new[]
                    {
                        new FieldContent(nameof(r.Code), r.Code),
                        new FieldContent(nameof(r.Description), r.Description), new FieldContent("ZUV", zuvText)
                    };
                    results.AddRow(content);
                }
            }

            var sections = new TableContent("Sections");

            var AllLectionHours = 0;
            var AllPracticeHours = 0;
            var AllSROHours = 0;
            var SectionNumber = 1;
            var SectionName = "";
            var SemesterNumber = "";
            var LectionHours = 0;
            var PracticeHours = 0;
            var SROHours = 0;
            var AllHours = 0;
            foreach (var section in disciplinePlanItem.Children
                .SelectMany(sem => sem.Children)
                .SelectMany(x => x.Children)
                .GroupBy(section => section.Knowledges[0].Name))
            {
                SectionName = section.Key;
                SemesterNumber = section.Select(x => x.Parent.Parent).FirstOrDefault().OrderNumber.ToString();
                LectionHours = section.Where(x => x.Parent.Type == "Лекции").FirstOrDefault().Hours;
                PracticeHours = section.Where(x => x.Parent.Type == "Практики").FirstOrDefault().Hours;
                SROHours = section.Where(x => x.Parent.Type == "СРС").FirstOrDefault().Hours;
                AllHours = LectionHours + PracticeHours + SROHours;
                AllLectionHours += LectionHours;
                AllPracticeHours += PracticeHours;
                AllSROHours += SROHours;
                var content = new[]
                {
                    new FieldContent(nameof(SectionNumber), SectionNumber.ToString()),
                    new FieldContent(nameof(SectionName), SectionName),
                    new FieldContent(nameof(SemesterNumber), SemesterNumber.ToString()),
                    new FieldContent(nameof(LectionHours), LectionHours.ToString()),
                    new FieldContent(nameof(PracticeHours), PracticeHours.ToString()),
                    new FieldContent(nameof(SROHours), SROHours.ToString()),
                    new FieldContent(nameof(AllHours), AllHours.ToString())
                };
                sections.AddRow(content);
                ++SectionNumber;
            }

            SectionName = "Всего";
            LectionHours = AllLectionHours;
            PracticeHours = AllPracticeHours;
            SROHours = AllSROHours;
            AllHours = LectionHours + PracticeHours + SROHours;

            var row = new[]
            {
                new FieldContent(nameof(SectionNumber), ""),
                new FieldContent(nameof(SectionName), SectionName),
                new FieldContent(nameof(SemesterNumber), ""),
                new FieldContent(nameof(LectionHours), LectionHours.ToString()),
                new FieldContent(nameof(PracticeHours), PracticeHours.ToString()),
                new FieldContent(nameof(SROHours), SROHours.ToString()),
                new FieldContent(nameof(AllHours), AllHours.ToString())
            };
            sections.AddRow(row);


            SectionName = "Контроль";
            AllHours = disciplinePlanItem.Children.Select(sem => ((SemesterItemDTO)sem).Exam).Sum();

            row = new[]
            {
                new FieldContent(nameof(SectionNumber), ""),
                new FieldContent(nameof(SectionName), SectionName),
                new FieldContent(nameof(SemesterNumber), ""),
                new FieldContent(nameof(LectionHours), ""),
                new FieldContent(nameof(PracticeHours), ""),
                new FieldContent(nameof(SROHours), ""),
                new FieldContent(nameof(AllHours), AllHours.ToString())
            };
            sections.AddRow(row);

            SectionName = "Итого";
            AllHours += LectionHours + PracticeHours + SROHours;

            row = new[]
            {
                new FieldContent(nameof(SectionNumber), ""),
                new FieldContent(nameof(SectionName), SectionName),
                new FieldContent(nameof(SemesterNumber), ""),
                new FieldContent(nameof(LectionHours), ""),
                new FieldContent(nameof(PracticeHours), ""),
                new FieldContent(nameof(SROHours), ""),
                new FieldContent(nameof(AllHours), AllHours.ToString())
            };
            sections.AddRow(row);

            var author = disciplineDto.Authors.FirstOrDefault();
            var Author = author.FirstName.Substring(0, 1) + ". " + (author.SecondName != null ? author.SecondName.Substring(0, 1) + ". " : "") + author.LastName;
            var CurrentYear = DateTime.Now.Year.ToString();
            var PreviousDisciplines = String.Join(", ", disciplineDto.PreviousDisciplines.Select(dis => dis.Name).ToArray());
            if (PreviousDisciplines != "")
                PreviousDisciplines =
                    "Дисциплина базируется на знаниях, полученных при изучении таких дисциплин, как " +
                    PreviousDisciplines;
            var NextDisciplines = String.Join(", ", disciplineDto.NextDisciplines.Select(dis => dis.Name).ToArray());
            if (NextDisciplines != "")
                NextDisciplines =
                    "Знания, полученные при изучении данной дисциплины, могут быть использованы при изучении таких дисциплин как " +
                    NextDisciplines;
            var semestersNumbers = disciplineService.GetSemestersNumbers(structureDepartmentId, academicPlan.Knowledges[0].Name, discipline);
            var Semesters = String.Join(", ", semestersNumbers);
            var courses = new HashSet<int>();
            foreach (var item in semestersNumbers)
            {
                courses.Add(item % 2 == 0 ? item / 2 : item / 2 + 1);
            }
            var Courses = String.Join(", ", courses);

            var valuesToFill = new Content(
                new FieldContent(nameof(discipline), discipline),
                new FieldContent(nameof(Author), Author),
                new FieldContent(nameof(CurrentYear), CurrentYear),
                new FieldContent(nameof(academicPlan.StartYear), academicPlan.StartYear.ToString()),
                new FieldContent(nameof(academicPlan.Direction), academicPlan.Direction.Replace("Направление", "")),
                new FieldContent(nameof(academicPlan.ModeOfStudy), academicPlan.ModeOfStudy),
                new FieldContent(nameof(academicPlan.Qualification), academicPlan.Qualification),
                new FieldContent(nameof(disciplineDto.Goals), disciplineDto.Goals.Replace("\n", "\r\n")),
                new FieldContent(nameof(disciplineDto.Tasks), disciplineDto.Tasks.Replace("\n", "\r\n")),
                new FieldContent(nameof(PreviousDisciplines), PreviousDisciplines),
                new FieldContent(nameof(NextDisciplines), NextDisciplines),
                new FieldContent(nameof(Semesters), Semesters),
                new FieldContent(nameof(Courses), Courses),
                results,
                sections
            );

            using (var outputDocument = new TemplateProcessor(AppDomain.CurrentDomain.BaseDirectory + @"\Rpds\" + filename)
                .SetRemoveContentControls(true))
            {
                outputDocument.FillContent(valuesToFill);
                outputDocument.SaveChanges();
            }

            return @"/Rpds/" + filename;
        }

        public void Dispose()
        {
        }
    }
}
