﻿using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IJobRoleService CreateJobRoleService()
        {
            return new JobRoleService(new IdentityUnitOfWork());
        }

        public IStructureDepartmentService CreateStructureDepartmentService()
        {
            return new StructureDepartmentService(new IdentityUnitOfWork());
        }

        public IUserService CreateUserService()
        {
            return new UserService(new IdentityUnitOfWork());
        }

        public IRoleService CreateRoleService()
        {
            return new RoleService(new IdentityUnitOfWork());
        }

        public IAcademicPlanService CreateAcademicPlanService()
        {
            return new AcademicPlanService(new IdentityUnitOfWork());
        }

        public IDocumentGeneratorService CreateDocumentGenerator(IDisciplineService disciplineService) => 
            new DocumentGeneratorService(disciplineService);

        public IDisciplineService CreateDisciplineService()
        {
            return new DisciplineService(new IdentityUnitOfWork());
        }

        public IRpdParserService CreateRpdParserService() =>
            new RpdParserService();
    }
}
