﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;

namespace EducationalProgramGenerator.BLL.Services
{
    public class ElectronicResourceGOSTTemplateEngine : IGOSTTemplateEngine
    {
        public string getGOSTString(LiteratureDTO literature)
        {
            var electronicResource = literature as ElectronicResourceDTO;
            string str = "";
            if (electronicResource.Title != "")
                str += electronicResource.Title;
            else
                return str;
            if (electronicResource.TitleInfo != "")
                str += " [Электронный ресурс] : " + electronicResource.TitleInfo;
            else
                return str;
            if (electronicResource.ModeOfAccess != "")
                str += ". - Режим доступа: " + electronicResource.ModeOfAccess;
            else
                return str;
            if (electronicResource.CirculationDate != "")
                str += " (дата обращения: " + electronicResource.CirculationDate + ").";
            return str;
        }
    }
}
