﻿using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    class RoleService : IRoleService
    {
        IUnitOfWork Database { get; set; }

        public RoleService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public IEnumerable<string> GetAll()
        {
            return Database.RoleManager.Roles.Select(r => r.Name).ToList();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public async Task UpdateAsync(string oldName, string newName)
        {
            var role = await Database.RoleManager.FindByNameAsync(oldName);
            role.Name = newName;
            await Database.RoleManager.UpdateAsync(role);
        }

        public async Task DeleteAsync(string roleName)
        {
            var role = await Database.RoleManager.FindByNameAsync(roleName);
            await Database.RoleManager.DeleteAsync(role);
        }

        public async Task CreateAsync(string roleName)
        {
            await Database.RoleManager.CreateAsync(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole { Name = roleName });
        }
    }
}
