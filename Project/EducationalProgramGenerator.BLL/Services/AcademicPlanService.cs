﻿using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EducationalProgramGenerator.BLL.Services
{
    public class AcademicPlanService : IAcademicPlanService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper;

        public AcademicPlanService(IUnitOfWork uow)
        {
            Database = uow;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
            cfg => { cfg.CreateMap<PlanItem, PlanItemDTO>()
                        .Include<AcademicPlanItem,AcademicPlanItemDTO>()
                        .Include<SemesterItem, SemesterItemDTO>()
                        .Include<DisciplineItem, DisciplineItemDTO>()
                        .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<KnowledgeItem, KnowledgeItemDTO>()
                    .Include<Discipline, DisciplineDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<LearningOutcomes, LearningOutcomesDTO>()
                    .Include<Competency, CompetencyDTO>()
                    .Include<DisciplineLO, DisciplineLODTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<AcademicPlanItem, AcademicPlanItemDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<DisciplineLO, DisciplineLODTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Competency, CompetencyDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Discipline, DisciplineDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Literature, LiteratureDTO>()
                    .Include<Book, BookDTO>()
                    .Include<ElectronicResource, ElectronicResourceDTO>()
                    .Include<OtherLiterature, OtherLiteratureDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Book, BookDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<ElectronicResource, ElectronicResourceDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<OtherLiterature, OtherLiteratureDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<ApplicationUser, UserDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<JobRole, JobRoleDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<StructureDepartment, StructureDepartmentDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Control, ControlDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<SemesterItem, SemesterItemDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<DisciplineItem, DisciplineItemDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
            });
            mapper = config.CreateMapper();
        }

        public void AddPlanItemChild(PlanItemDTO parent, PlanItemDTO child)
        {
            parent.Children.Add(child);
            child.Parent = parent;
            AddHours(parent, child.Hours);
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        private void AddHours(PlanItemDTO planItem, int hours)
        {
            planItem.Hours += hours;
            if (planItem.Parent != null)
                AddHours(planItem.Parent, hours);
        }

        public IEnumerable<PlanItemDTO> GetPlanItemChildren(PlanItemDTO parent)
        {
            return parent.Children;
        }

        public void SaveAcademicPlan(XDocument document, int structureDepartmentId)
        {
            var academicPlanDTO = Parse(document);
            academicPlanDTO.StructureDepartmentId = structureDepartmentId;
            Database.CRUDRepository.Create(mapper.Map<AcademicPlanItemDTO, AcademicPlanItem>(academicPlanDTO));
            Database.Save();
        }

        public List<string> GetAcademicPlansNames(int structureDepartmentId)
        {
            return Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId).SelectMany(plan => plan.Knowledges)
                .Select(knowledge => knowledge.Name).ToList();
        }

        public List<AcademicPlanItemDTO> GetAcademicPlans(int structureDepartmentId)
        {
            var academicPlans = Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId).ToList();
            return mapper.Map<List<AcademicPlanItemDTO>>(academicPlans);
        }

        public List<string> GetDisciplinesNames(int structureDepartmentId, string plan)
        {
            return Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId).SelectMany(p => p.Knowledges)
                .Where(knowledge => knowledge.Name == plan)
                .SelectMany(knowledge => knowledge.Children)
                .Select(knowledge => knowledge.Name)
                .ToList();
        }

        public int GetStartYear(int structureDepartmentId, string plan)
        {
            return GetAcademicPlanQuery(structureDepartmentId, plan).First().StartYear;
        }

        private IQueryable<AcademicPlanItem> GetAcademicPlanQuery(int structureDepartmentId, string plan)
        {
            return Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId && x.Knowledges.FirstOrDefault().Name == plan);
        }

        private IQueryable<PlanItem> GetDisciplineQuery(int structureDepartmentId, string plan, string discipline)
        {   
            return GetAcademicPlanQuery(structureDepartmentId, plan).SelectMany(p => p.Children)
                .Where(dis => dis.Knowledges.FirstOrDefault().Name == discipline);
        }

        public List<LiteratureDTO> GetDisciplineLiterature(int structureDepartmentId, string plan, string discipline)
        {
            var literature = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .Select(x => x.Knowledges.FirstOrDefault())
                .SelectMany(x => (x as Discipline).Literature)
                .ToList();
            return mapper.Map<List<LiteratureDTO>>(literature);
        }

        public List<string> GetPreviousDisciplinesNames(int structureDepartmentId, string plan, string discipline)
        {
            var endSemesterNumber = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Children)
                .Max(sem => sem.OrderNumber);
            return GetAcademicPlanQuery(structureDepartmentId, plan).SelectMany(p => p.Children)
                .Where(dis => dis.Children.Min(sem => sem.OrderNumber) < endSemesterNumber
                && dis.Knowledges.FirstOrDefault().Name != discipline)
                .Select(dis => dis.Knowledges.FirstOrDefault().Name)
                .ToList();
        }

        public List<string> GetNextDisciplinesNames(int structureDepartmentId, string plan, string discipline)
        {
            var startSemesterNumber = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Children)
                .Min(sem => sem.OrderNumber);
            return GetAcademicPlanQuery(structureDepartmentId, plan).SelectMany(p => p.Children)
                .Where(dis => dis.Children.Max(sem => sem.OrderNumber) > startSemesterNumber 
                && dis.Knowledges.FirstOrDefault().Name != discipline)
                .Select(dis => dis.Knowledges.FirstOrDefault().Name)
                .ToList();
        }

        public List<string> GetDisciplineCompetenciesNames(int structureDepartmentId, string plan, string discipline)
        {
            return GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Knowledges.FirstOrDefault().Results)
                .Select(result => (result as Competency).Code + ": " + result.Description)
                .ToList();
        }

        public List<string> GetDisciplineCompetenciesCodes(int structureDepartmentId, string plan, string discipline)
        {
            var t = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Knowledges.FirstOrDefault().Results)
                .ToList()
                .Select(result => result.GetType()).ToList();
            return GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Knowledges.FirstOrDefault().Results)
                .ToList()
                .Where(result => result.GetType().BaseType == typeof(Competency))
                .Select(result => (result as Competency).Code)
                .ToList();
        }

        public List<string> GetDisciplineCompetenciesDescriptions(int structureDepartmentId, string plan, string discipline)
        {
            return GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Knowledges.FirstOrDefault().Results)
                .ToList()
                .Where(result => result.GetType().BaseType == typeof(Competency))
                .Select(result => result.Description)
                .ToList();
        }

        public List<PlanItemDTO> GetDisciplineSemesters(int structureDepartmentId, string plan, string discipline)
        {
            var semesters = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(p => p.Children)
                .Where(item => item.Type == "Семестр")
                .ToList();
            return mapper.Map<List<PlanItem>, List<PlanItemDTO>>(semesters);
        }

        public AcademicPlanItemDTO GetAcademicPlan(int structureDepartmentId, string plan)
        {
            var academicPlan = Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId && x.Knowledges.FirstOrDefault().Name == plan)
                .FirstOrDefault();
            return mapper.Map<AcademicPlanItem, AcademicPlanItemDTO>(academicPlan);
        }

        public List<CompetencyDTO> GetDisciplineCompetencies(int structureDepartmentId, string plan, string discipline)
        {
            var competencies = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(dis => dis.Knowledges.FirstOrDefault().Results)
                .Select(result => result as Competency).ToList();
            return mapper.Map<IEnumerable<Competency>, List<CompetencyDTO>>(competencies);
        }

        public PlanItemDTO GetDiscipline(int structureDepartmentId, string plan, string discipline)
        {
            return mapper.Map<PlanItemDTO>(GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault());
        }

        private AcademicPlanItemDTO Parse(XDocument document)
        {
            var academicPlanXml = document;
            var planStrings = academicPlanXml.Element("Документ")
                .Element("План")
                .Element("СтрокиПлана")
                .Elements("Строка");

            var direction = academicPlanXml.Element("Документ")
                .Element("План")
                .Element("Титул")
                .Element("Специальности")
                .Element("Специальность")
                .Attribute("Название").Value;

            var qualification = academicPlanXml.Element("Документ")
                .Element("План")
                .Attribute("ОбразовательнаяПрограмма").Value.Split(' ')[1];
            qualification = qualification.Remove(qualification.Length - 2);

            var modeOfStudy = academicPlanXml.Element("Документ")
                .Element("План")
                .Attribute("ФормаОбучения").Value;

            var startYear = int.Parse(academicPlanXml.Element("Документ")
                .Element("План")
                .Element("Титул")
                .Attribute("ГодНачалаПодготовки").Value);

            var plan = new AcademicPlanItemDTO("Учебный план", direction, qualification, modeOfStudy, startYear);

            var educationalProgram = new KnowledgeItemDTO(direction + " " + startYear + " (" + qualification + ", " + modeOfStudy + ")");

            plan.Knowledges.Add(educationalProgram);

            var competencyDescriptions = academicPlanXml
                .Element("Документ")
                .Element("План")
                .Element("Компетенции")
                .Elements("Строка")
                .ToDictionary(c => c.Attribute("Индекс").Value.Trim(), c => new CompetencyDTO(c.Attribute("Содержание").Value, c.Attribute("Индекс").Value.Trim()));

            foreach (var str in planStrings)
            {
                var disKnowledge = new DisciplineDTO(str.Attribute("Дис").Value);
                disKnowledge.Code = str.Attribute("НовИдДисциплины") != null 
                    ? str.Attribute("НовИдДисциплины").Value : str.Attribute("ИдетификаторДисциплины").Value;
                var dis = new DisciplineItemDTO("Дисциплина");
                //Рассредоточенные практики
                if (str.Attribute("ЧасовВЗЕТ") != null)
                    dis.HoursInZet = int.Parse(str.Attribute("ЧасовВЗЕТ").Value);
                else
                {
                    //тут ещё надо покекать
                    if (str.Attribute("ТипПрактики") != null)
                        continue;
                }
                //Элективные курсы по физре
                if (str.Attribute("КредитовНаДисциплину") != null)
                    dis.Zet = int.Parse(str.Attribute("КредитовНаДисциплину").Value);
                else
                {
                    dis.HoursInZet = 0;
                    dis.Gos = str.Attribute("ГОС") != null ? int.Parse(str.Attribute("ГОС").Value) : int.Parse(str.Attribute("ПодлежитИзучению").Value);
                }

                dis.Knowledges.Add(disKnowledge);

                var competencies = str.Attribute("Компетенции").Value.Split(',');
                foreach (var competency in competencies)
                    disKnowledge.Results.Add(competencyDescriptions[competency.Trim()]);

                foreach (var semester in str.Elements("Сем"))
                {
                    var sem = new SemesterItemDTO("Семестр", orderNumber: int.Parse(semester.Attribute("Ном").Value), 
                        zet: semester.Attribute("ЗЕТ") != null ? int.Parse(semester.Attribute("ЗЕТ").Value) : 0,
                        exam: semester.Attribute("ЧасЭкз") != null ? int.Parse(semester.Attribute("ЧасЭкз").Value) : 0,
                        pass: semester.Attribute("Зач") != null ? true : false,
                        gradedTest: semester.Attribute("ЗачО") != null ? true : false,
                        courseWork: semester.Attribute("КР") != null ? true : false);
                    AddPlanItemChild(dis, sem);
                    var practice = semester.Attribute("Пр") != null
                        ? new PlanItemDTO("Практики", int.Parse(semester.Attribute("Пр").Value))
                        : new PlanItemDTO("Практики");
                    var srs = semester.Attribute("СРС") != null
                        ? new PlanItemDTO("СРС", int.Parse(semester.Attribute("СРС").Value))
                        : new PlanItemDTO("СРС");
                    var lectures = semester.Attribute("Лек") != null
                        ? new PlanItemDTO("Лекции", int.Parse(semester.Attribute("Лек").Value))
                        : new PlanItemDTO("Лекции");
                    AddPlanItemChild(sem, practice);
                    AddPlanItemChild(sem, srs);
                    AddPlanItemChild(sem, lectures);
                    sem.Hours += sem.Exam;
                }
                educationalProgram.Children.Add(disKnowledge);
                AddPlanItemChild(plan, dis);
            }
            return plan;
        }
    }
}
