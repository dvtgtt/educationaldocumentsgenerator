﻿using EducationalProgramGenerator.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.DTO;
using System.IO;
using System.Text.RegularExpressions;
using EducationalProgramGenerator.DAL.Entities;
using Iveonik.Stemmers;
using Spire.Doc;

namespace EducationalProgramGenerator.BLL.Services
{
    public class RpdParserService : IRpdParserService
    {

        private Dictionary<string, string> keywords = new Dictionary<string, string>
        {
            {"цел дисциплин", "tasks goals"},
            {"задач дисциплин", "tasks goals"},
            //{"базов", "discipline place"},
            {"след", "discipline place"},
            {"мест дисциплин", "discipline place"},
            {"предшествова", "discipline place"},
            {"базирова", "discipline place"},
            {"структур дисциплин", "structure"},
            //{"тем", "structure"},
            //{"раздел", "structure"},
            {"лекц", "structure"},
            {"практик", "structure"},
            {"срс", "structure"},
            {"сро", "structure"},
            {"час", "structure"},
            {"компетенц", "learning outcomes"},
            {"результат обучен", "learning outcomes"},
            {"литератур", "literature"}
        };

        private Dictionary<string, List<string>> sections = new Dictionary<string, List<string>>
        {
            {"title", new List<string>()},
            {"tasks goals", new List<string>()},
            {"discipline place", new List<string>()},
            {"structure", new List<string>()},
            {"learning outcomes", new List<string>()},
            {"literature", new List<string>()}
        };

        private Dictionary<string, int> sectionPriorities = new Dictionary<string, int>
        {
            {"title", 0},
            {"tasks goals", 1},
            {"discipline place", 2},
            {"structure", 3},
            {"learning outcomes", 2},
            {"literature", 4}
        };

        IStemmer stemmer = new RussianStemmer();

        private void splitSections(string text)
        {
            var currentSection = "title";

            var isContentList = false;
            var contentList = new List<string>();
            foreach (var paragraph in text.Split(new[] { "\r\n" }, StringSplitOptions.None))
            {
                if (isContentList && (!contentList.Any() || !contentList[0].Contains(paragraph)))
                {
                    contentList.Add(paragraph);
                    continue;
                }

                if (isContentList && contentList.Count() != 0 && contentList[0].Contains(paragraph))
                    isContentList = false;

                if (paragraph.Trim() == "Содержание" && currentSection == "title")
                {
                    isContentList = true;
                    continue;
                }

                var tokens = Regex.Matches(paragraph, @"\w+");
                bool isCurrent = false;
                var currentToken = "";
                foreach (Match firstToken in tokens)
                {
                    foreach (Match secondToken in tokens)
                    {
                        var token = stemmer.Stem(firstToken.Value) == stemmer.Stem(secondToken.Value)
                            ? stemmer.Stem(firstToken.Value)
                            : stemmer.Stem(firstToken.Value) + " " + stemmer.Stem(secondToken.Value);
                        if (keywords.ContainsKey(token)
                            && sectionPriorities[keywords[token]] == sectionPriorities[currentSection])
                        {
                            currentToken = token;
                            isCurrent = true;
                            break;
                        }
                        if (keywords.ContainsKey(token)
                            && (sectionPriorities[keywords[token]] - sectionPriorities[currentSection]) == 1)
                        {
                            currentToken = token;
                        }
                    }
                    if (isCurrent) break;
                }
                if (currentToken != "") currentSection = keywords[currentToken];
                sections[currentSection].Add(paragraph);
            }
        }

        private List<LearningOutcomesDTO> getLearningOutcomes()
        {
            var learningOutcomes = new List<LearningOutcomesDTO>();
            var competencyCode = new Regex(@"[А-Я]+-[0-9]+");
            var currentField = "";

            CompetencyDTO competency = null;
            foreach (var lo in sections["learning outcomes"])
            {
                if (lo.Trim() == "")
                    continue;
                if (competencyCode.IsMatch(lo))
                {
                    competency = new CompetencyDTO(competencyCode.Replace(lo, ""), competencyCode.Match(lo).Value);
                    learningOutcomes.Add(competency);
                    currentField = "Описание";
                    continue;
                }
                if (lo.Contains("Знать"))
                    currentField = "Знать";
                if (lo.Contains("Уметь"))
                    currentField = "Уметь";
                if (lo.Contains("Владеть"))
                    currentField = "Владеть";

                if (currentField != "" && lo.Replace(currentField, "").Trim().Length < 2)
                    continue;
                switch (currentField)
                {
                    case "Знать":
                        learningOutcomes.Add(new DisciplineLODTO(lo.Replace("Знать:", "").Replace("- ", "").Trim())
                        {
                            Competency = competency,
                            Type = "Знать"
                        });
                        break;
                    case "Уметь":
                        learningOutcomes.Add(new DisciplineLODTO(lo.Replace("Уметь:", "").Replace("- ", "").Trim())
                        {
                            Competency = competency,
                            Type = "Уметь"
                        });
                        break;
                    case "Владеть":
                        learningOutcomes.Add(new DisciplineLODTO(lo.Replace("Владеть:", "").Replace("- ", "").Trim())
                        {
                            Competency = competency,
                            Type = "Владеть"
                        });
                        break;
                    case "Описание":
                        competency.Description += " " + lo;
                        break;
                }
            }
            return learningOutcomes;
        }

        private string getGoals() => string.Join("\r\n", string.Join(". ", sections["tasks goals"]).Split('.')
            .Where(i => !Regex.IsMatch(i, @"\d") && i.Trim() != "")
            .Skip(1)
            .TakeWhile(i => !(Regex.Matches(i, @"\w+")
                                  .Cast<Match>()
                                  .Select(token => stemmer.Stem(token.Value.ToLower()))
                                  .Contains("задач")
                              && (Regex.Matches(i, @"\w+")
                                      .Cast<Match>()
                                      .Select(token => stemmer.Stem(token.Value.ToLower()))
                                      .Contains("дисциплин")
                                  || Regex.Matches(i, @"\w+")
                                      .Cast<Match>()
                                      .Select(token => stemmer.Stem(token.Value.ToLower()))
                                      .Contains("курс")))
                            || Regex.Matches(i, @"\w+")
                                .Cast<Match>()
                                .Select(token => stemmer.Stem(token.Value.ToLower()))
                                .Contains("цел")));

        private string getTasks() => string.Join("\r\n", string.Join(". ", sections["tasks goals"]).Split('.')
            .Where(i => !Regex.IsMatch(i, @"\d") && i.Trim() != "")
            .Skip(1)
            .SkipWhile(i => !(Regex.Matches(i, @"\w+")
                                  .Cast<Match>()
                                  .Select(token => stemmer.Stem(token.Value.ToLower()))
                                  .Contains("задач")
                              && (Regex.Matches(i, @"\w+")
                                      .Cast<Match>()
                                      .Select(token => stemmer.Stem(token.Value.ToLower()))
                                      .Contains("дисциплин")
                                  || Regex.Matches(i, @"\w+")
                                      .Cast<Match>()
                                      .Select(token => stemmer.Stem(token.Value.ToLower()))
                                      .Contains("курс")))
                            || Regex.Matches(i, @"\w+")
                                .Cast<Match>()
                                .Select(token => stemmer.Stem(token.Value.ToLower()))
                                .Contains("цел")));

        private List<LiteratureDTO> getLiterature()
        {
            bool isMain = false;
            bool isOptional = false;
            bool isOther = false;

            var literature = new List<LiteratureDTO>();
            foreach (var i in sections["literature"])
            {
                if (i.Trim() == "")
                    continue;

                var tokens = Regex.Matches(i.ToLower(), @"\w+").Cast<Match>().Select(token => token.Value).ToList();
                if (tokens.Contains("основная") && tokens.Contains("литература"))
                    isMain = true;
                if (isMain && i.ToLower().Contains("дополнительная"))
                {
                    isMain = false;
                    isOptional = true;
                    continue;
                }
                if (isOptional && !Regex.IsMatch(i.Trim(), @"\d\.? .+"))
                {
                    isOptional = false;
                }

                if (isMain && Regex.IsMatch(i.Trim(), @"\d\.? .+"))
                    literature.Add(new OtherLiteratureDTO
                    {
                        Description = Regex.Replace(i, @"^[^A-Za-zА-Яа-я]+", ""),
                        Section = "Основная литература"
                    });
                    

                if (isOptional && Regex.IsMatch(i.Trim(), @"\d\.? .+"))
                    literature.Add(new OtherLiteratureDTO
                    {
                        Description = Regex.Replace(i, @"^[^A-Za-zА-Яа-я]+", ""),
                        Section = "Дополнительная литература"
                    });

                if (!isOptional && !isMain && i.Contains("[Электронный ресурс]"))
                    literature.Add(new OtherLiteratureDTO
                    {
                        Description = Regex.Replace(i, @"^[^A-Za-zА-Яа-я]+", ""),
                        Section = "Электронные ресурсы сети Интернет"
                    });
            }
            return literature;
        }

        private List<string> findDisciplines(List<string> disciplines) => disciplines.Where(dis => string.Join(" ", sections["discipline place"]).Contains(dis)).ToList();

        public void Dispose()
        {
        }

        public PlanItemDTO Parse(Stream inputStream, List<string> previousDisciplines, List<string> nextDisciplines)
        {
            var document = new Document();
            document.LoadFromStream(inputStream, FileFormat.Auto);
            splitSections(document.GetText());

            var planItem = new PlanItemDTO("Дисциплина");
            var discipline = new DisciplineDTO("");
            planItem.Knowledges.Add(discipline);

            discipline.Tasks = getTasks();
            discipline.Goals = getGoals();
            discipline.Literature = getLiterature();
            discipline.Results.AddRange(getLearningOutcomes());
            discipline.PreviousDisciplines = findDisciplines(previousDisciplines).Select(dis => new DisciplineDTO(dis))
                .ToList();
            discipline.NextDisciplines = findDisciplines(nextDisciplines).Select(dis => new DisciplineDTO(dis))
                .ToList();

            return planItem;
        }
    }
}
