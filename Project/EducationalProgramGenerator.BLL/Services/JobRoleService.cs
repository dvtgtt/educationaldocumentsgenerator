﻿using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace EducationalProgramGenerator.BLL.Services
{
    public class JobRoleService : IJobRoleService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper;

        public JobRoleService(IUnitOfWork uow)
        {
            Database = uow;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
            cfg => cfg.CreateMap<JobRole, JobRoleDTO>().ReverseMap());
            mapper = config.CreateMapper();
        }

        public IEnumerable<JobRoleDTO> GetAll()
        {
            return mapper.Map<IEnumerable<JobRole>, List<JobRoleDTO>>(Database.CRUDRepository.Select<JobRole>());
        }

        public JobRoleDTO Get(int id)
        {
            return mapper.Map<JobRole, JobRoleDTO>(Database.CRUDRepository.Get<JobRole>(id));
        }

        public void Update(JobRoleDTO jobRoleDTO)
        {
            Database.CRUDRepository.Update(mapper.Map<JobRoleDTO, JobRole>(jobRoleDTO));
            Database.Save();
        }

        public void Delete(int id)
        {
            var jobRole = Database.CRUDRepository.Select<JobRole>().Where(x => x.Id == id).FirstOrDefault();
            Database.CRUDRepository.Delete(jobRole);
            Database.Save();
        }

        public void Create(JobRoleDTO jobRoleDTO)
        {
            Database.CRUDRepository.Create(mapper.Map<JobRoleDTO, JobRole>(jobRoleDTO));
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
