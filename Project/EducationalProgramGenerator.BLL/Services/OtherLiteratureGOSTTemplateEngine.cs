﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;

namespace EducationalProgramGenerator.BLL.Services
{
    public class OtherLiteratureGOSTTemplateEngine : IGOSTTemplateEngine
    {
        public string getGOSTString(LiteratureDTO literature)
        {
            var otherLiterature = literature as OtherLiteratureDTO;
            string str = "";
            if (otherLiterature.Description != "")
                str += otherLiterature.Description;
            return str;
        }
    }
}
