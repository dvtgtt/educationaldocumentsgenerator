﻿using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Infrastructure;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper;

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
            cfg =>
            {
                cfg.CreateMap<ApplicationUser, UserDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<JobRole, JobRoleDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<StructureDepartment, StructureDepartmentDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
            });
            mapper = config.CreateMapper();
        }

        public async Task<Tuple<OperationDetails, ClaimsIdentity>> Register(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            ClaimsIdentity claimIdentity = null;
            if (user == null)
            {
                StructureDepartment structureDepartment = Database.CRUDRepository.
                    Select<StructureDepartment>().Where(x => x.Id == userDto.StructureDepartmentId).FirstOrDefault();
                user = new ApplicationUser
                {
                    UserName = userDto.Email,
                    Email = userDto.Email,
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    SecondName = userDto.SecondName,
                    StructureDepartment = structureDepartment
                };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new Tuple<OperationDetails, ClaimsIdentity>
                        (new OperationDetails(false, result.Errors.FirstOrDefault(), ""), claimIdentity);
                await Database.UserManager.AddToRoleAsync(user.Id, "user");
                claimIdentity = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
                await Database.SaveAsync();
                return new Tuple<OperationDetails, ClaimsIdentity>
                    (new OperationDetails(true, "Регистрация успешно пройдена", ""), claimIdentity);
            }
            else
            {
                return new Tuple<OperationDetails, ClaimsIdentity>
                    (new OperationDetails(false, "Пользователь с таким логином уже существует", "Email"), claimIdentity);
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            await Database.SaveAsync();
            return claim;
        }

        public async Task<UserDTO> FindByIdAsync(string id)
        {
            return mapper.Map<ApplicationUser, UserDTO>(await Database.UserManager.FindByIdAsync(id));
        }

        public UserDTO FindById(string id)
        {
            return mapper.Map<ApplicationUser, UserDTO>(Database.UserManager.FindById(id));
        }

        public async Task<IList<string>> GetRolesAsync(string userId)
        {
            return await Database.UserManager.GetRolesAsync(userId);
        }

        public bool IsInRole(string userId, string roleName)
        {
            return Database.UserManager.IsInRole(userId, roleName);
        }

        public List<UserDTO> GetAll()
        {
            return mapper.Map<IEnumerable<ApplicationUser>, List<UserDTO>>(Database.UserManager.Users.ToList());
        }

        public async Task<OperationDetails> RemoveFromRoleAsync(string userId, string roleName)
        {
            var result = await Database.UserManager.RemoveFromRoleAsync(userId, roleName);
            await Database.SaveAsync();
            return new OperationDetails(result.Succeeded, "", "");
        }

        public async Task<OperationDetails> AddToRoleAsync(string userId, string roleName)
        {
            var result = await Database.UserManager.AddToRoleAsync(userId, roleName);
            await Database.SaveAsync();
            return new OperationDetails(result.Succeeded, "", "");
        }

        public OperationDetails ChangeStructureDepartment(string userId, int structureDepartmentId)
        {
            var user = Database.UserManager.FindById(userId);
            var structureDepartment = Database.CRUDRepository.Get<StructureDepartment>(structureDepartmentId);
            if (structureDepartment == null)
                return new OperationDetails(false, "", "");
            user.StructureDepartment = structureDepartment;
            Database.UserManager.Update(user);
            Database.Save();
            return new OperationDetails(true, "", "");
        }

        public OperationDetails ChangeJobRole(string userId, int jobRoleId)
        {
            var user = Database.UserManager.FindById(userId);
            var jobRole = Database.CRUDRepository.Get<JobRole>(jobRoleId);
            if (jobRole == null)
                return new OperationDetails(false, "", "");
            user.JobRole = jobRole;
            Database.UserManager.Update(user);
            Database.Save();
            return new OperationDetails(true, "", "");
        }

        public async Task<OperationDetails> ChangeFullNameAsync(string userId, string firstName, string secondName, string lastName)
        {
            var user = await Database.UserManager.FindByIdAsync(userId);
            user.LastName = lastName;
            user.FirstName = firstName;
            user.SecondName = secondName;
            var result = await Database.UserManager.UpdateAsync(user);
            await Database.SaveAsync();
            return new OperationDetails(result.Succeeded, "", "");
        }

        public async Task<OperationDetails> ChangeEmailAsync(string userId, string email)
        {
            var user = await Database.UserManager.FindByIdAsync(userId);
            user.Email = email;
            user.UserName = email;
            var result = await Database.UserManager.UpdateAsync(user);
            await Database.SaveAsync();
            return new OperationDetails(result.Succeeded, "", "");
        }

        public async Task<Tuple<OperationDetails, ClaimsIdentity>> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
        {
            ClaimsIdentity claimIdentity = null;
            var result = await Database.UserManager.ChangePasswordAsync(userId, oldPassword, newPassword);
            if (result.Errors.Count() > 0)
                return new Tuple<OperationDetails, ClaimsIdentity>
                    (new OperationDetails(false, result.Errors.FirstOrDefault(), ""), claimIdentity);
            var user = await Database.UserManager.FindByIdAsync(userId);
            claimIdentity = await Database.UserManager.CreateIdentityAsync(user,
                                        DefaultAuthenticationTypes.ApplicationCookie);
            await Database.SaveAsync();
            return new Tuple<OperationDetails, ClaimsIdentity>
                (new OperationDetails(true, "Пароль изменен", ""), claimIdentity);
        }

        public async Task<Tuple<OperationDetails, ClaimsIdentity>> SetPasswordAsync(string userId, string newPassword)
        {
            ClaimsIdentity claimIdentity = null;
            await Database.UserManager.RemovePasswordAsync(userId);
            var result = await Database.UserManager.AddPasswordAsync(userId, newPassword);
            if (result.Errors.Count() > 0)
                return new Tuple<OperationDetails, ClaimsIdentity>
                    (new OperationDetails(false, result.Errors.FirstOrDefault(), ""), claimIdentity);
            var user = await Database.UserManager.FindByIdAsync(userId);
            claimIdentity = await Database.UserManager.CreateIdentityAsync(user,
                                        DefaultAuthenticationTypes.ApplicationCookie);
            await Database.SaveAsync();
            return new Tuple<OperationDetails, ClaimsIdentity>
                (new OperationDetails(true, "Пароль установлен", ""), claimIdentity);
        }

        public UserDTO FindByUserName(string userName)
        {
            return mapper.Map<UserDTO>(Database.UserManager.FindByEmail(userName));
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        public async Task DeleteAsync(string email)
        {
            var user = await Database.UserManager.FindByEmailAsync(email);
            await Database.UserManager.DeleteAsync(user);
        }
    }
}
