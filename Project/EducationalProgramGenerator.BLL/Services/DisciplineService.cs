﻿using AutoMapper;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    public class DisciplineService : IDisciplineService
    {
        IUnitOfWork Database { get; set; }
        IMapper mapper;

        public DisciplineService(IUnitOfWork uow)
        {
            Database = uow;
            ConfigureMapper();
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
            cfg => {
                cfg.CreateMap<PlanItem, PlanItemDTO>()
                   .Include<AcademicPlanItem, AcademicPlanItemDTO>()
                   .Include<SemesterItem, SemesterItemDTO>()
                   .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<KnowledgeItem, KnowledgeItemDTO>()
                    .Include<Discipline, DisciplineDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<LearningOutcomes, LearningOutcomesDTO>()
                    .Include<Competency, CompetencyDTO>()
                    .Include<DisciplineLO, DisciplineLODTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<AcademicPlanItem, AcademicPlanItemDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Competency, CompetencyDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Discipline, DisciplineDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Literature, LiteratureDTO>()
                    .Include<Book, BookDTO>()
                    .Include<ElectronicResource, ElectronicResourceDTO>()
                    .Include<OtherLiterature, OtherLiteratureDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Book, BookDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<ElectronicResource, ElectronicResourceDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<OtherLiterature, OtherLiteratureDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<DisciplineLO, DisciplineLODTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<ApplicationUser, UserDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<JobRole, JobRoleDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<StructureDepartment, StructureDepartmentDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<Control, ControlDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
                cfg.CreateMap<SemesterItem, SemesterItemDTO>()
                    .PreserveReferences().ReverseMap().MaxDepth(500);
            });
            mapper = config.CreateMapper();
        }

        public void Dispose()
        {
            Database.Dispose();
        }

        private IQueryable<AcademicPlanItem> GetAcademicPlanQuery(int structureDepartmentId, string plan)
        {
            return Database.CRUDRepository.Select<AcademicPlanItem>()
                .Where(x => x.StructureDepartmentId == structureDepartmentId && x.Knowledges.FirstOrDefault().Name == plan);
        }

        private IQueryable<PlanItem> GetDisciplineQuery(int structureDepartmentId, string plan, string discipline)
        {
            return GetAcademicPlanQuery(structureDepartmentId, plan).SelectMany(p => p.Children)
                .Where(dis => dis.Knowledges.FirstOrDefault().Name == discipline);
        }

        public LiteratureDTO GetLiteratureItem(int id)
        {
            var literatureItem = Database.CRUDRepository.Select<Literature>()
                .Where(x => x.Id == id)
                .FirstOrDefault();
            return mapper.Map<LiteratureDTO>(literatureItem);
        }

        public void RemoveLiteratureItem(int id)
        {
            var literatureItem = Database.CRUDRepository.Select<Literature>()
                .Where(x => x.Id == id)
                .FirstOrDefault();
            Database.CRUDRepository.Delete(literatureItem);
            Database.Save();
        }

        public void SaveLiteratureItem(int structureDepartmentId, string plan, string discipline, LiteratureDTO literatureItem)
        {
            var db_discipline = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Knowledges)
                .Select(x => (x as Discipline))
                .FirstOrDefault();
            var db_literatureItem = mapper.Map<Literature>(literatureItem);
            if (db_literatureItem.Id != 0)
            {
                //Database.CRUDRepository.Attach(db_literatureItem);
                //Database.CRUDRepository.Update(db_literatureItem);
                var original = Database.CRUDRepository.Get<Literature>(db_literatureItem.Id);
                Database.CRUDRepository.SetValues(original, db_literatureItem);
            }
            else
            {
                Database.CRUDRepository.Create(db_literatureItem);
            }
            if (db_discipline.Literature.Where(x => x.Id == db_literatureItem.Id).FirstOrDefault() == null)
            {
                db_discipline.Literature.Add(db_literatureItem);
            }
            Database.Save();
        }

        public PlanItemDTO GetPlanItem(int id)
        {
            var planItem = Database.CRUDRepository.Get<PlanItem>(id);
            return mapper.Map<PlanItemDTO>(planItem);
        }

        public Dictionary<string, int> GetRemainingHours(int structureDepartmentId, string plan, string discipline, int semesterNumber)
        {            
            var childrenHours = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Children)
                .Where(sem => sem.OrderNumber == semesterNumber)
                .SelectMany(x => x.Children)
                .SelectMany(c => c.Children)
                .GroupBy(x => x.Parent.Type)
                .ToDictionary(x => x.Key, x => x.Select(h => h.Hours).Sum());

            var hours = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Children)
                .Where(sem => sem.OrderNumber == semesterNumber)
                .SelectMany(x => x.Children)
                .ToDictionary(x => x.Type, x => x.Hours - (childrenHours.ContainsKey(x.Type) ? childrenHours[x.Type] : 0));
            return hours;
        }

        public void SaveSection(int structureDepartmentId, string plan, string discipline, int semesterNumber,
            PlanItemDTO lectureDTO, PlanItemDTO practiceDTO, PlanItemDTO srsDTO, KnowledgeItemDTO sectionDTO)
        {
            var semester = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Children)
                .Where(sem => sem.OrderNumber == semesterNumber)
                .FirstOrDefault();
            var semesterLecture = semester.Children.Where(x => x.Type == "Лекции").FirstOrDefault();
            var semesterPractice = semester.Children.Where(x => x.Type == "Практики").FirstOrDefault();
            var semesterSrs = semester.Children.Where(x => x.Type == "СРС").FirstOrDefault();
            semesterLecture.Children.Add(mapper.Map<PlanItem>(lectureDTO));
            semesterPractice.Children.Add(mapper.Map<PlanItem>(practiceDTO));
            semesterSrs.Children.Add(mapper.Map<PlanItem>(srsDTO));
            Database.Save();
        }

        public void AddDisciplineLOToSection(int disciplineLOId, int sectionId)
        {
            var section1 = Database.CRUDRepository.Get<PlanItem>(sectionId);
            var section2 = section1.Parent.Parent.Children[1]
                .Children.Where(x => x.Knowledges[0].Name == section1.Knowledges[0].Name).FirstOrDefault();
            var section3 = section1.Parent.Parent.Children[2]
                .Children.Where(x => x.Knowledges[0].Name == section1.Knowledges[0].Name).FirstOrDefault();
            var disciplineLO = Database.CRUDRepository.Get<DisciplineLO>(disciplineLOId);

            section1.Knowledges[0].Results.Add(disciplineLO);
            section2.Knowledges[0].Results.Add(disciplineLO);
            section3.Knowledges[0].Results.Add(disciplineLO);
            Database.Save();
        }

        public void RemoveDisciplineLOFromSection(int disciplineLOId, int sectionId)
        {
            var section1 = Database.CRUDRepository.Get<PlanItem>(sectionId);
            var section2 = section1.Parent.Parent.Children[1]
                .Children.Where(x => x.Knowledges[0].Name == section1.Knowledges[0].Name).FirstOrDefault();
            var section3 = section1.Parent.Parent.Children[2]
                .Children.Where(x => x.Knowledges[0].Name == section1.Knowledges[0].Name).FirstOrDefault();
            var disciplineLO = Database.CRUDRepository.Get<DisciplineLO>(disciplineLOId);
            section1.Knowledges[0].Results.Remove(disciplineLO);
            section2.Knowledges[0].Results.Remove(disciplineLO);
            section3.Knowledges[0].Results.Remove(disciplineLO);
            Database.Save();
        }

        public void SavePlanItem(PlanItemDTO planItem, int sectionId = 0)
        {
            var db_planItem = mapper.Map<PlanItem>(planItem);
            if (planItem.Type == "Практика" || planItem.Type == "СРС")
            {
                if (planItem.Knowledges[0].Literature != null)
                {
                    db_planItem.Knowledges[0].Literature = new List<Literature>();
                    foreach (var item in planItem.Knowledges[0].Literature)
                    {
                        var literature = Database.CRUDRepository.Get<Literature>(item.Id);
                        db_planItem.Knowledges[0].Literature.Add(literature);
                    }
                }
            }
            if (planItem.Type == "СРС")
            {
                var control = Database.CRUDRepository.Select<Control>().Where(x => x.Name == planItem.Control.Name).FirstOrDefault();
                db_planItem.Control = control ?? new Control { Name = planItem.Control.Name };
            }
            if (db_planItem.Id > 0)
            {
                var original = Database.CRUDRepository.Get<PlanItem>(db_planItem.Id);
                db_planItem.Knowledges[0].Id = original.Knowledges[0].Id;
                if (planItem.Type == "СРС")
                    original.Control = db_planItem.Control;
                if (planItem.Type == "Практика" || planItem.Type == "СРС" || planItem.Type == "Дисциплина")
                {
                    original.Knowledges[0].Literature.Clear();
                    foreach (var item in db_planItem.Knowledges[0].Literature)
                    {
                        original.Knowledges[0].Literature.Add(item);
                    }
                }
                if (planItem.Type == "Дисциплина")
                {
                    original.Knowledges[0].Results.Clear();
                    foreach (var item in db_planItem.Knowledges[0].Results)
                    {
                        original.Knowledges[0].Results.Add(item);
                    }

                    (original.Knowledges[0] as Discipline).PreviousDisciplines.Clear();
                    foreach (var item in (db_planItem.Knowledges[0] as Discipline).PreviousDisciplines)
                    {
                        (original.Knowledges[0] as Discipline).PreviousDisciplines.Add(item);
                    }

                    (original.Knowledges[0] as Discipline).NextDisciplines.Clear();
                    foreach (var item in (db_planItem.Knowledges[0] as Discipline).NextDisciplines)
                    {
                        (original.Knowledges[0] as Discipline).NextDisciplines.Add(item);
                    }
                }
                Database.CRUDRepository.SetValues(original.Knowledges[0], db_planItem.Knowledges[0]);
                Database.CRUDRepository.SetValues(original, db_planItem);
            }
            else
            {
                var section = Database.CRUDRepository.Get<PlanItem>(sectionId);
                db_planItem.Parent = section;
                db_planItem.OrderNumber = Database.CRUDRepository.Select<PlanItem>().Count() + 1;
                Database.CRUDRepository.Create(db_planItem);
            }
            Database.Save();
        }

        public void RemovePlanItem(int id)
        {
            var planItem = Database.CRUDRepository.Get<PlanItem>(id);
            foreach (var knowledge in planItem.Knowledges.ToArray())
            {
                Database.CRUDRepository.Delete(knowledge);
            }
            Database.CRUDRepository.Delete(planItem);
            Database.Save();
        }

        public void AddGoals(int structureDepartmentId, string plan, string discipline, string goals)
        {
            var dis = (Discipline) GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            dis.Goals = goals;
            Database.Save();
        }

        public void AddTasks(int structureDepartmentId, string plan, string discipline, string tasks)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            dis.Tasks = tasks;
            Database.Save();
        }

        public void AddPreviousDiscipline(int structureDepartmentId, string plan, string discipline, string previous)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            var previousDis =
                (Discipline) GetDisciplineQuery(structureDepartmentId, plan, previous).FirstOrDefault().Knowledges[0];
            dis.PreviousDisciplines.Add(previousDis);
            Database.Save();
        }

        public void AddNextDiscipline(int structureDepartmentId, string plan, string discipline, string next)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            var nextDis =
                (Discipline)GetDisciplineQuery(structureDepartmentId, plan, next).FirstOrDefault().Knowledges[0];
            dis.NextDisciplines.Add(nextDis);
            Database.Save();
        }

        public void RemovePreviousDiscipline(int structureDepartmentId, string plan, string discipline, string previous)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            var previousDis =
                (Discipline)GetDisciplineQuery(structureDepartmentId, plan, previous).FirstOrDefault().Knowledges[0];
            dis.PreviousDisciplines.Remove(previousDis);
            Database.Save();
        }

        public void RemoveNextDiscipline(int structureDepartmentId, string plan, string discipline, string next)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            var nextDis =
                (Discipline)GetDisciplineQuery(structureDepartmentId, plan, next).FirstOrDefault().Knowledges[0];
            dis.NextDisciplines.Remove(nextDis);
            Database.Save();
        }

        public string GetGoals(int structureDepartmentId, string discipline, string plan)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            return dis.Goals;
        }

        public string GetTasks(int structureDepartmentId, string discipline, string plan)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            return dis.Tasks;
        }

        public List<string> GetPreviousDisciplines(int structureDepartmentId, string discipline, string plan)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            return dis.PreviousDisciplines.Select(item => item.Name).ToList();
        }

        public List<string> GetNextDisciplines(int structureDepartmentId, string discipline, string plan)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            return dis.NextDisciplines.Select(item => item.Name).ToList();
        }

        public List<DisciplineLODTO> GetDisciplineLOs(int structureDepartmentId, string discipline, string plan, string type, string competencyCode)
        {
            var dis = (Discipline)GetDisciplineQuery(structureDepartmentId, plan, discipline).FirstOrDefault().Knowledges[0];
            return dis.Results
                    .Where(result => result.GetType().BaseType == typeof(DisciplineLO))
                    .Cast<DisciplineLO>()
                    .Where(result => result.Competency.Code == competencyCode && result.Type == type)
                    .Select(result => mapper.Map<DisciplineLODTO>(result))
                    .ToList();
        }

        public DisciplineLODTO GetDisciplineLO(int id)
        {
            var learningOutcomes = Database.CRUDRepository.Get<DisciplineLO>(id);
            return mapper.Map<DisciplineLODTO>(learningOutcomes);
        }

        public void SaveDisciplineLO(int structureDepartmentId, string plan, string discipline, string competency, DisciplineLODTO disciplineLO)
        {
            var db_disciplineLO = mapper.Map<DisciplineLO>(disciplineLO);
            db_disciplineLO.Competency = Database.CRUDRepository
                .Select<Competency>()
                .Where(c => c.Code == competency)
                .FirstOrDefault();
            if (db_disciplineLO.Id > 0)
            {
                //Database.CRUDRepository.Attach(db_literatureItem);
                //Database.CRUDRepository.Update(db_literatureItem);
                var original = Database.CRUDRepository.Get<DisciplineLO>(db_disciplineLO.Id);
                Database.CRUDRepository.SetValues(original, db_disciplineLO);
            }
            else
            {
                Database.CRUDRepository.Create(db_disciplineLO);

                GetDisciplineQuery(structureDepartmentId, plan, discipline)
                    .FirstOrDefault()
                    .Knowledges[0]
                    .Results.Add(db_disciplineLO);
            }
            Database.Save();
        }

        public void RemoveLearningOutcomes(int id)
        {
            var disciplineLO = Database.CRUDRepository.Get<DisciplineLO>(id);
            Database.CRUDRepository.Delete(disciplineLO);
            Database.Save();
        }

        public List<UserDTO> GetAuthors(int structureDepartmentId, string plan, string discipline)
        {
            var disciplineAuthors = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => (x.Knowledges.FirstOrDefault() as Discipline).Authors)
                .ToList();
            return mapper.Map<List<UserDTO>>(disciplineAuthors);
        }

        public void AddAuthor(int structureDepartmentId, string plan, string discipline, string id)
        {
            var db_discipline = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Knowledges)
                .Select(x => (x as Discipline))
                .FirstOrDefault();
            var author = Database.CRUDRepository.Get<ApplicationUser>(id);
            db_discipline.Authors.Add(author);
            Database.Save();
        }
        public void RemoveAuthor(int structureDepartmentId, string plan, string discipline, string id)
        {
            var db_discipline = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Knowledges)
                .Select(x => (x as Discipline))
                .FirstOrDefault();
            var author = Database.CRUDRepository.Get<ApplicationUser>(id);
            db_discipline.Authors.Remove(author);
            Database.Save();
        }

        public List<int> GetSemestersNumbers(int structureDepartmentId, string plan, string discipline)
        {
            return GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Children)
                .Select(sem => sem.OrderNumber)
                .ToList();
        }

        //Полнейший кек
        public List<ControlDTO> GetControls()
        {
            var controls = Database.CRUDRepository.Select<Control>().ToList();
            return mapper.Map<List<ControlDTO>>(controls);
        }

        public List<LiteratureDTO> GetLiterature(int structureDepartmentId, string plan, string discipline)
        {
            var literature = GetDisciplineQuery(structureDepartmentId, plan, discipline)
                .SelectMany(x => x.Knowledges)
                .SelectMany(x => x.Literature)
                .ToList();
            return mapper.Map<List<LiteratureDTO>>(literature);
        }

        public List<PlanItemDTO> GetUserRPDs(string username)
        {
            var user = Database.CRUDRepository.Select<ApplicationUser>().Where(x => x.UserName == username).FirstOrDefault();
            var rpds = Database.CRUDRepository.Select<PlanItem>()
                .Where(x => x is AcademicPlanItem)
                .SelectMany(x => x.Children)
                .Where(x => (x.Knowledges.FirstOrDefault() as Discipline).Authors.Any(y => y.Id == user.Id))
                .ToList();
            return mapper.Map<List<PlanItemDTO>>(rpds);
        }
    }
}
