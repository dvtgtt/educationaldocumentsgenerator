﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IDisciplineService : IDisposable
    {
        LiteratureDTO GetLiteratureItem(int id);
        void SaveLiteratureItem(int structureDepartmentId, string plan, string discipline, LiteratureDTO literatureItem);
        void RemoveLiteratureItem(int id);
        PlanItemDTO GetPlanItem(int id);
        void SavePlanItem(PlanItemDTO planItem, int sectionId = 0);
        void RemovePlanItem(int id);
        Dictionary<string, int> GetRemainingHours(int structureDepartmentId, string plan, string discipline, int semesterNumber);
        void AddGoals(int structureDepartmentId, string plan, string discipline, string goals);
        void AddTasks(int structureDepartmentId, string plan, string discipline, string tasks);
        void AddPreviousDiscipline(int structureDepartmentId, string plan, string discipline, string previous);
        void AddNextDiscipline(int structureDepartmentId, string plan, string discipline, string next);
        void RemovePreviousDiscipline(int structureDepartmentId, string plan, string discipline, string previous);
        void RemoveNextDiscipline(int structureDepartmentId, string plan, string discipline, string next);
        string GetGoals(int structureDepartmentId, string discipline, string plan);
        string GetTasks(int structureDepartmentId, string discipline, string plan);
        List<string> GetPreviousDisciplines(int structureDepartmentId, string discipline, string plan);
        List<string> GetNextDisciplines(int structureDepartmentId, string discipline, string plan);

        List<DisciplineLODTO> GetDisciplineLOs(int structureDepartmentId, string discipline, string plan, string type,
            string competencyCode);

        DisciplineLODTO GetDisciplineLO(int id);

        void SaveDisciplineLO(int structureDepartmentId, string plan, string discipline, string competency,
            DisciplineLODTO disciplineLO);

        void RemoveLearningOutcomes(int id);

        void SaveSection(int structureDepartmentId, string plan, string discipline, int semesterNumber,
            PlanItemDTO lectureDTO, PlanItemDTO practiceDTO, PlanItemDTO srsDTO, KnowledgeItemDTO sectionDTO);

        void AddDisciplineLOToSection(int disciplineLOId, int sectionId);
        void RemoveDisciplineLOFromSection(int disciplineLOId, int sectionId);
        List<UserDTO> GetAuthors(int structureDepartmentId, string plan, string discipline);
        void AddAuthor(int structureDepartmentId, string plan, string discipline, string id);
        void RemoveAuthor(int structureDepartmentId, string plan, string discipline, string id);
        List<int> GetSemestersNumbers(int structureDepartmentId, string plan, string discipline);
        List<ControlDTO> GetControls();
        List<PlanItemDTO> GetUserRPDs(string username);
        List<LiteratureDTO> GetLiterature(int structureDepartmentId, string plan, string discipline);
    }
}
