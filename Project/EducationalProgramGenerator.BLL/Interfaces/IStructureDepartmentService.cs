﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IStructureDepartmentService : IDisposable
    {
        IEnumerable<StructureDepartmentDTO> GetAll();
        StructureDepartmentDTO Get(int id);
        void Update(StructureDepartmentDTO jobRoleDTO);
        void Delete(int id);
        void Create(StructureDepartmentDTO jobRoleDTO);
    }
}
