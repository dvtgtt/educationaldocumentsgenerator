﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IRpdParserService : IDisposable
    {
        PlanItemDTO Parse(Stream inputStream, List<string> previousDisciplines, List<string> nextDisciplines);
    }
}
