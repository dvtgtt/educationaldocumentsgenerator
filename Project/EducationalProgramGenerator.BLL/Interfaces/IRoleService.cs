﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IRoleService : IDisposable
    {
        IEnumerable<string> GetAll();
        Task UpdateAsync(string oldName, string newName);
        Task DeleteAsync(string roleName);
        Task CreateAsync(string roleName);
    }
}
