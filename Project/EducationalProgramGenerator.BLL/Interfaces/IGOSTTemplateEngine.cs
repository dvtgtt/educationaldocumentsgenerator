﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Services
{
    public interface IGOSTTemplateEngine
    {
        string getGOSTString(LiteratureDTO literature);
    }
}
