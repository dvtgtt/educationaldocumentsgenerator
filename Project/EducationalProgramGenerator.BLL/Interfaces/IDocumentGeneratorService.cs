﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.DTO;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IDocumentGeneratorService : IDisposable
    {
        string CreateDocument(AcademicPlanItemDTO academicPlan, PlanItemDTO disciplinePlanItem, int structureDepartmentId);
    }
}
