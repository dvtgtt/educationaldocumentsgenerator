﻿using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<Tuple<OperationDetails, ClaimsIdentity>> Register(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task<UserDTO> FindByIdAsync(string id);
        UserDTO FindById(string id);
        Task<IList<string>> GetRolesAsync(string userId);
        bool IsInRole(string userId, string roleName);
        List<UserDTO> GetAll();
        Task<OperationDetails> RemoveFromRoleAsync(string userId, string roleName);
        Task<OperationDetails> AddToRoleAsync(string userId, string roleName);
        OperationDetails ChangeStructureDepartment(string userId, int structureDepartmentId);
        OperationDetails ChangeJobRole(string userId, int jobRoleId);
        Task<OperationDetails> ChangeFullNameAsync(string userId, string firstName, string secondName, string lastName);
        Task<OperationDetails> ChangeEmailAsync(string userId, string email);
        Task<Tuple<OperationDetails, ClaimsIdentity>> ChangePasswordAsync(string userId, string oldPassword, string newPassword);
        Task<Tuple<OperationDetails, ClaimsIdentity>> SetPasswordAsync(string userId, string newPassword);
        UserDTO FindByUserName(string userName);
        Task DeleteAsync(string email);
    }
}
