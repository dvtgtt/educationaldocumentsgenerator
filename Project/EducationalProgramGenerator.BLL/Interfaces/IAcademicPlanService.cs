﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IAcademicPlanService : IDisposable
    {
        void AddPlanItemChild(PlanItemDTO parent, PlanItemDTO child);
        IEnumerable<PlanItemDTO> GetPlanItemChildren(PlanItemDTO parent);
        void SaveAcademicPlan(XDocument document, int structureDepartmentId);
        List<string> GetAcademicPlansNames(int structureDepartmentId);
        List<string> GetDisciplinesNames(int structureDepartmentId, string plan);
        List<string> GetPreviousDisciplinesNames(int structureDepartmentId, string plan, string discipline);
        List<string> GetNextDisciplinesNames(int structureDepartmentId, string plan, string discipline);
        List<string> GetDisciplineCompetenciesNames(int structureDepartmentId, string plan, string discipline);
        List<PlanItemDTO> GetDisciplineSemesters(int structureDepartmentId, string plan, string discipline);
        AcademicPlanItemDTO GetAcademicPlan(int structureDepartmentId, string plan);
        List<CompetencyDTO> GetDisciplineCompetencies(int structureDepartmentId, string plan, string discipline);
        List<LiteratureDTO> GetDisciplineLiterature(int structureDepartmentId, string plan, string discipline);
        int GetStartYear(int structureDepartmentId, string plan);
        List<string> GetDisciplineCompetenciesCodes(int structureDepartmentId, string plan, string discipline);
        List<string> GetDisciplineCompetenciesDescriptions(int structureDepartmentId, string plan, string discipline);
        PlanItemDTO GetDiscipline(int structureDepartmentId, string plan, string discipline);
        List<AcademicPlanItemDTO> GetAcademicPlans(int structureDepartmentId);
    }
}
