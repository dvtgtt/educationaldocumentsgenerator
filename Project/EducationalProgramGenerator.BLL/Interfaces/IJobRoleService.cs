﻿using EducationalProgramGenerator.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IJobRoleService : IDisposable
    {
        IEnumerable<JobRoleDTO> GetAll();
        JobRoleDTO Get(int id);
        void Update(JobRoleDTO jobRoleDTO);
        void Delete(int id);
        void Create(JobRoleDTO jobRoleDTO);
        /*IEnumerable<StructureDepartmentDTO> Find(Func<StructureDepartmentDTO, Boolean> predicate);
        void Create(StructureDepartmentDTO item);
        void Update(StructureDepartmentDTO item);
        void Delete(int id);*/
    }
}
