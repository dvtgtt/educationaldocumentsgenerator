﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Interfaces
{
    public interface IServiceCreator
    {
        IUserService CreateUserService();
        IStructureDepartmentService CreateStructureDepartmentService();
        IJobRoleService CreateJobRoleService();
        IRoleService CreateRoleService();
        IAcademicPlanService CreateAcademicPlanService();
        IDocumentGeneratorService CreateDocumentGenerator(IDisciplineService disciplineService);
        IDisciplineService CreateDisciplineService();
        IRpdParserService CreateRpdParserService();
    }
}
