﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class SemesterItemDTO : PlanItemDTO
    {
        public int Exam { get; set; }
        public bool Pass { get; set; }
        public bool GradedTest { get; set; }
        public bool CourseWork { get; set; }

        public SemesterItemDTO(string type, int orderNumber = 0, int zet = 0, int hours = 0, int exam = 0, bool pass = false, bool gradedTest = false, bool courseWork = false) 
            : base(type, hours, orderNumber, zet)
        {
            Exam = exam;
            Pass = pass;
            GradedTest = gradedTest;
            CourseWork = courseWork;
        }
    }
}
