﻿using EducationalProgramGenerator.BLL.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class LiteratureDTO
    {
        [HtmlAnnotation(Visible = false)]
        public int Id { get; set; }
        public string Section { get; set; }
    }
}
