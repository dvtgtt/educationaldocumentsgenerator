﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class CompetencyDTO : LearningOutcomesDTO
    {
        public readonly string Code;
        public CompetencyDTO(string description, string code) : base(description)
        {
            Code = code;
        }
    }
}
