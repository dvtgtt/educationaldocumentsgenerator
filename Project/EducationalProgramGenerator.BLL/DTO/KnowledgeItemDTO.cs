﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.Annotations;
using System.ComponentModel.DataAnnotations;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class KnowledgeItemDTO
    {
        public int Id { get; set; }
        [Required]
        [HtmlAnnotation(Visible = true, Placeholder = "", Name = "Название")]
        public string Name { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "", Name = "Содержание", InputTag = "textarea")]
        public string Description { get; set; }
        public List<KnowledgeItemDTO> RequiredKnoledges { get; private set; }
        public List<KnowledgeItemDTO> Children { get; private set; }
        public List<LearningOutcomesDTO> Results { get; private set; }
        public List<LiteratureDTO> Literature { get; set; }

        public KnowledgeItemDTO(string name, string description = "")
        {
            Name = name;
            Description = description;
            RequiredKnoledges = new List<KnowledgeItemDTO>();
            Children = new List<KnowledgeItemDTO>();
            Results = new List<LearningOutcomesDTO>();
            Literature = new List<LiteratureDTO>();
        }
    }
}
