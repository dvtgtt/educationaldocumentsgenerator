﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.Annotations;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class DisciplineLODTO : LearningOutcomesDTO
    {
        [HtmlAnnotation(Visible = false)]
        public string Type { get; set; }
        public virtual CompetencyDTO Competency { get; set; }

        public DisciplineLODTO(string description) : base(description)
        {
        }
    }
}
