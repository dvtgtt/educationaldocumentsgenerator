﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class StructureDepartmentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Type { get; set; }
        public virtual List<StructureDepartmentDTO> Children { get; set; }
        public virtual StructureDepartmentDTO Parent { get; set; }
    }
}
