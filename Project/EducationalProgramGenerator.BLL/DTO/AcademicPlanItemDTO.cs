﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class AcademicPlanItemDTO : PlanItemDTO
    {
        public int StructureDepartmentId { get; set; }
        public StructureDepartmentDTO StructureDepartment { get; set; }
        public string Direction { get; private set; }
        public string Qualification { get; private set; }
        public string ModeOfStudy { get; private set; }
        public int StartYear { get; private set; }

        public AcademicPlanItemDTO(string type, string direction, string qualification, string modeOfStudy, int startYear, int hours = 0) : base(type, hours)
        {
            Direction = direction;
            Qualification = qualification;
            ModeOfStudy = modeOfStudy;
            StartYear = startYear;
        }
    }
}
