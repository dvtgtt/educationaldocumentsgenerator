﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.Annotations;
using System.ComponentModel.DataAnnotations;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class PlanItemDTO
    {
        [HtmlAnnotation(Visible = false)]
        public int Id { get; set; }
        [HtmlAnnotation(Visible = false)]
        public int OrderNumber { get; set; }
        [Required]
        [HtmlAnnotation(Visible = true, Placeholder = "Количество академических часов", Name = "Часы", IsNumber = true)]
        public int Hours { get; internal set; }
        [HtmlAnnotation(Visible = false)]
        public string Type { get; set; }
        public List<PlanItemDTO> Children { get; set; }
        public List<KnowledgeItemDTO> Knowledges { get; set; }
        public PlanItemDTO Parent;
        public ControlDTO Control { get; set; }
        public int Zet { get; set; }

        public PlanItemDTO(string type, int hours = 0, int orderNumber = 0, int zet = 0)
        {
            Children = new List<PlanItemDTO>();
            Hours = hours;
            Type = type;
            Knowledges = new List<KnowledgeItemDTO>();
            OrderNumber = orderNumber;
            Zet = zet;
        }
    }
}
