﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class DisciplineItemDTO : PlanItemDTO
    {
        public DisciplineItemDTO(string type, int hours = 0, int orderNumber = 0, int zet = 0) : base(type, hours, orderNumber, zet)
        {
        }

        public int HoursInZet { get; set; }
        public int Gos { get; set; }
    }
}
