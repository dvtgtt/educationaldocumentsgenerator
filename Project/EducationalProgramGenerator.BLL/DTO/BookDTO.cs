﻿using EducationalProgramGenerator.BLL.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class BookDTO : LiteratureDTO
    {
        [HtmlAnnotation(Visible = true, Placeholder = "Фамилия И.О., Фамилия И.О.", Name = "Автор")]
        public string Author { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "Название книги", Name = "Основное заглавие")]
        public string Title { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "учебник, справочник и др.", Name = "Сведения о книге")]
        public string TitleInfo { get; set; }
        public string Responsible { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "3-е изд., перераб. и доп.", Name = "Сведения о переиздании")]
        public string Reissue { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "Город", Name = "Место издания")]
        public string City { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "Название издательства", Name = "Издательство")]
        public string PublishingHouse { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "2013", Name = "Год издания")]
        public string Year { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "Кол-во страниц", Name = "Объем")]
        public string Size { get; set; }
    }
}
