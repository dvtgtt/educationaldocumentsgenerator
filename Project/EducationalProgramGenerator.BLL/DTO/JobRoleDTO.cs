﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class JobRoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
