﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.Annotations;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class LearningOutcomesDTO
    {
        [HtmlAnnotation(Visible = false)]
        public int Id { get; set; }
        [HtmlAnnotation(Visible = true, Placeholder = "", Name = "Описание", InputTag = "textarea")]
        public string Description { get; set; }
        public List<LearningOutcomesDTO> Children { get; private set; }

        public LearningOutcomesDTO(string description)
        {
            Description = description;
            Children = new List<LearningOutcomesDTO>();
        }
    }
}
