﻿using EducationalProgramGenerator.BLL.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class ElectronicResourceDTO : LiteratureDTO
    {
        [Required]
        [Display(Name = "Название ресурса")]
        [HtmlAnnotation(Visible = true, Name = "Название ресурса", Placeholder = "Экономическая теория")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Сведения о ресурсе")]
        [HtmlAnnotation(Visible = true, Name = "Сведения о ресурсе", Placeholder = "сайт, портал ...")]
        public string TitleInfo { get; set; }
        [Required]
        [Display(Name = "Ссылка на ресурс")]
        [RegularExpression(@"([http://|https://|www\.]).*\..*",
            ErrorMessage = "Введите верный URL")]
        [HtmlAnnotation(Visible = true, Name = "Ссылка на ресурс", Placeholder = "http://google.com")]
        public string ModeOfAccess { get; set; }
        [Required]
        [Display(Name = "Дата обращения")]
        [RegularExpression(@"(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.20\d\d",
            ErrorMessage = "Введите дату 21-го века в формате дд.мм.гггг")]
        [HtmlAnnotation(Visible = true, Name = "Дата обращения", Placeholder = "05.12.2015")]
        public string CirculationDate { get; set; }
    }
}
