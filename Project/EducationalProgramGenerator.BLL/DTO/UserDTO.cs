﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public int StructureDepartmentId { get; set; }
        public int JobRoleId { get; set; }
        public JobRoleDTO JobRole { get; set; }
        public StructureDepartmentDTO StructureDepartment { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
    }
}
