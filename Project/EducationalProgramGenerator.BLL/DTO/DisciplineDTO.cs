﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class DisciplineDTO : KnowledgeItemDTO
    {
        //public string AuthorId { get; set; }
        public List<UserDTO> Authors { get; set; }
        public DisciplineDTO(string name, string description = "") : base(name, description)
        {
        }
        public string Goals { get; set; }
        public string Tasks { get; set; }
        public List<DisciplineDTO> PreviousDisciplines { get; set; }
        public List<DisciplineDTO> NextDisciplines { get; set; }
        public string Code { get; set; }
    }
}
