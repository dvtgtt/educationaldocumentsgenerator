﻿using EducationalProgramGenerator.BLL.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.DTO
{
    public class OtherLiteratureDTO : LiteratureDTO
    {
        [HtmlAnnotation(Visible = true, Name = "Описание", Placeholder = "Описание ресурса")]
        public string Description { get; set; }
    }
}
