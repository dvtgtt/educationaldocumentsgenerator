﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.BLL.Annotations
{
    public class HtmlAnnotationAttribute : System.Attribute
    {
        public bool Visible { get; set; }
        public string Placeholder { get; set; }
        public string Name { get; set; }
        public string InputTag { get; set; }
        public bool IsNumber { get; set; }
    }
}
