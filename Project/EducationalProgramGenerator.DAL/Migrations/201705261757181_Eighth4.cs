namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eighth4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ElectronicResources", "Title", c => c.String());
            AddColumn("dbo.ElectronicResources", "TitleInfo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ElectronicResources", "TitleInfo");
            DropColumn("dbo.ElectronicResources", "Title");
        }
    }
}
