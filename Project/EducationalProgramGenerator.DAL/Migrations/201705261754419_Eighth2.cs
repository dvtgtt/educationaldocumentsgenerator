namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eighth2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Literatures", "Title1");
            DropColumn("dbo.Literatures", "TitleInfo1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Literatures", "TitleInfo1", c => c.String());
            AddColumn("dbo.Literatures", "Title1", c => c.String());
        }
    }
}
