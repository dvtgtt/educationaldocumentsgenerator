namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eighth : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Literatures", "KnowledgeItem_Id", "dbo.KnowledgeItems");
            DropIndex("dbo.Literatures", new[] { "KnowledgeItem_Id" });
            CreateTable(
                "dbo.LiteratureKnowledgeItems",
                c => new
                    {
                        Literature_Id = c.Int(nullable: false),
                        KnowledgeItem_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Literature_Id, t.KnowledgeItem_Id })
                .ForeignKey("dbo.Literatures", t => t.Literature_Id, cascadeDelete: true)
                .ForeignKey("dbo.KnowledgeItems", t => t.KnowledgeItem_Id, cascadeDelete: true)
                .Index(t => t.Literature_Id)
                .Index(t => t.KnowledgeItem_Id);
            
            DropColumn("dbo.Literatures", "KnowledgeItem_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Literatures", "KnowledgeItem_Id", c => c.Int());
            DropForeignKey("dbo.LiteratureKnowledgeItems", "KnowledgeItem_Id", "dbo.KnowledgeItems");
            DropForeignKey("dbo.LiteratureKnowledgeItems", "Literature_Id", "dbo.Literatures");
            DropIndex("dbo.LiteratureKnowledgeItems", new[] { "KnowledgeItem_Id" });
            DropIndex("dbo.LiteratureKnowledgeItems", new[] { "Literature_Id" });
            DropTable("dbo.LiteratureKnowledgeItems");
            CreateIndex("dbo.Literatures", "KnowledgeItem_Id");
            AddForeignKey("dbo.Literatures", "KnowledgeItem_Id", "dbo.KnowledgeItems", "Id");
        }
    }
}
