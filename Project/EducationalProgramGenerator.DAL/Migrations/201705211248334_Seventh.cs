namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seventh : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Controls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PlanItems", "Control_Id", c => c.Int());
            CreateIndex("dbo.PlanItems", "Control_Id");
            AddForeignKey("dbo.PlanItems", "Control_Id", "dbo.Controls", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanItems", "Control_Id", "dbo.Controls");
            DropIndex("dbo.PlanItems", new[] { "Control_Id" });
            DropColumn("dbo.PlanItems", "Control_Id");
            DropTable("dbo.Controls");
        }
    }
}
