namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Thirteenth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SemesterItems", "Exam", c => c.Int(nullable: false));
            AddColumn("dbo.SemesterItems", "Pass", c => c.Boolean(nullable: false));
            AddColumn("dbo.SemesterItems", "GradedTest", c => c.Boolean(nullable: false));
            DropColumn("dbo.SemesterItems", "Number");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SemesterItems", "Number", c => c.Int(nullable: false));
            DropColumn("dbo.SemesterItems", "GradedTest");
            DropColumn("dbo.SemesterItems", "Pass");
            DropColumn("dbo.SemesterItems", "Exam");
        }
    }
}
