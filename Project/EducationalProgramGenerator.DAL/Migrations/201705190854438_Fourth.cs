namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fourth : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Controls", "Id", "dbo.PlanItems");
            DropIndex("dbo.Controls", new[] { "Id" });
            DropPrimaryKey("dbo.Controls");
            AddColumn("dbo.PlanItems", "Control_Id", c => c.Int());
            AddColumn("dbo.Controls", "Name", c => c.String());
            AlterColumn("dbo.Controls", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Controls", "Id");
            CreateIndex("dbo.PlanItems", "Control_Id");
            AddForeignKey("dbo.PlanItems", "Control_Id", "dbo.Controls", "Id");
            DropColumn("dbo.Controls", "Content");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Controls", "Content", c => c.String());
            DropForeignKey("dbo.PlanItems", "Control_Id", "dbo.Controls");
            DropIndex("dbo.PlanItems", new[] { "Control_Id" });
            DropPrimaryKey("dbo.Controls");
            AlterColumn("dbo.Controls", "Id", c => c.Int(nullable: false));
            DropColumn("dbo.Controls", "Name");
            DropColumn("dbo.PlanItems", "Control_Id");
            AddPrimaryKey("dbo.Controls", "Id");
            CreateIndex("dbo.Controls", "Id");
            AddForeignKey("dbo.Controls", "Id", "dbo.PlanItems", "Id");
        }
    }
}
