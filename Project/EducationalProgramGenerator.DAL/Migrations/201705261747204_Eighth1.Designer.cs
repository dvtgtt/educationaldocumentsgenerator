// <auto-generated />
namespace EducationalProgramGenerator.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Eighth1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Eighth1));
        
        string IMigrationMetadata.Id
        {
            get { return "201705261747204_Eighth1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
