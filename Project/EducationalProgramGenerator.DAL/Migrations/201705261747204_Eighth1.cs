namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eighth1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ElectronicResourceOutputs", "ElectronicResource_Id", "dbo.Literatures");
            DropForeignKey("dbo.ElectronicResourceSeries", "ElectronicResource_Id", "dbo.Literatures");
            DropForeignKey("dbo.ElectronicResourceSystemRequirements", "ElectronicResource_Id", "dbo.Literatures");
            DropIndex("dbo.ElectronicResourceOutputs", new[] { "ElectronicResource_Id" });
            DropIndex("dbo.ElectronicResourceSeries", new[] { "ElectronicResource_Id" });
            DropIndex("dbo.ElectronicResourceSystemRequirements", new[] { "ElectronicResource_Id" });
            DropColumn("dbo.Literatures", "EditionInfo");
            DropColumn("dbo.Literatures", "ParallelEditionInfo");
            DropColumn("dbo.Literatures", "AdditionalEditionInfoResponsible");
            DropColumn("dbo.Literatures", "Type");
            DropColumn("dbo.Literatures", "Size1");
            DropColumn("dbo.Literatures", "ManufacturerPlace");
            DropColumn("dbo.Literatures", "ManufacturerName");
            DropColumn("dbo.Literatures", "ManufactureDate");
            DropColumn("dbo.Literatures", "PhysicalMediumInfo");
            DropColumn("dbo.Literatures", "OtherPhysicalMediumInfo");
            DropColumn("dbo.Literatures", "PhysicalMediumSize");
            DropColumn("dbo.Literatures", "AccompanyingMaterial");
            DropColumn("dbo.Literatures", "StandartNumber");
            DropColumn("dbo.Literatures", "StandartKeyTitle");
            DropColumn("dbo.Literatures", "StandartAccess");
            DropTable("dbo.ElectronicResourceOutputs");
            DropTable("dbo.ElectronicResourceSeries");
            DropTable("dbo.ElectronicResourceSystemRequirements");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ElectronicResourceSystemRequirements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ComputerNameAndModel = c.String(),
                        RAM = c.String(),
                        OS = c.String(),
                        Software = c.String(),
                        Peripherals = c.String(),
                        TechnicalMeans = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectronicResourceSeries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ISSN = c.String(),
                        Number = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ElectronicResourceOutputs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EditionPlace = c.String(),
                        AndOtherEditionPlaces = c.Boolean(nullable: false),
                        AndOtherEditionNames = c.Boolean(nullable: false),
                        Date = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Literatures", "StandartAccess", c => c.String());
            AddColumn("dbo.Literatures", "StandartKeyTitle", c => c.String());
            AddColumn("dbo.Literatures", "StandartNumber", c => c.String());
            AddColumn("dbo.Literatures", "AccompanyingMaterial", c => c.String());
            AddColumn("dbo.Literatures", "PhysicalMediumSize", c => c.String());
            AddColumn("dbo.Literatures", "OtherPhysicalMediumInfo", c => c.String());
            AddColumn("dbo.Literatures", "PhysicalMediumInfo", c => c.String());
            AddColumn("dbo.Literatures", "ManufactureDate", c => c.String());
            AddColumn("dbo.Literatures", "ManufacturerName", c => c.String());
            AddColumn("dbo.Literatures", "ManufacturerPlace", c => c.String());
            AddColumn("dbo.Literatures", "Size1", c => c.String());
            AddColumn("dbo.Literatures", "Type", c => c.String());
            AddColumn("dbo.Literatures", "AdditionalEditionInfoResponsible", c => c.String());
            AddColumn("dbo.Literatures", "ParallelEditionInfo", c => c.String());
            AddColumn("dbo.Literatures", "EditionInfo", c => c.String());
            CreateIndex("dbo.ElectronicResourceSystemRequirements", "ElectronicResource_Id");
            CreateIndex("dbo.ElectronicResourceSeries", "ElectronicResource_Id");
            CreateIndex("dbo.ElectronicResourceOutputs", "ElectronicResource_Id");
            AddForeignKey("dbo.ElectronicResourceSystemRequirements", "ElectronicResource_Id", "dbo.Literatures", "Id");
            AddForeignKey("dbo.ElectronicResourceSeries", "ElectronicResource_Id", "dbo.Literatures", "Id");
            AddForeignKey("dbo.ElectronicResourceOutputs", "ElectronicResource_Id", "dbo.Literatures", "Id");
        }
    }
}
