namespace EducationalProgramGenerator.DAL.Migrations
{
    using EducationalProgramGenerator.DAL.Entities;
    using EducationalProgramGenerator.DAL.Identity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EducationalProgramGenerator.DAL.EF.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "EducationalProgramGenerator.DAL.EF.ApplicationContext";
        }

        protected override void Seed(EducationalProgramGenerator.DAL.EF.ApplicationContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (userManager.Users.Count() == 0)
            {
                // создаем две роли
                var role1 = new IdentityRole { Name = "admin" };
                var role2 = new IdentityRole { Name = "user" };
                var role3 = new IdentityRole { Name = "superuser" };

                // добавляем роли в бд
                roleManager.Create(role1);
                roleManager.Create(role2);
                roleManager.Create(role3);

                var CSU = context.StructureDepartment.Add(new StructureDepartment
                {
                    Name = "Челябинский государственный университет",
                    ShortName = "ЧелГУ",
                    Type = "Университет"
                });
                var IIT = context.StructureDepartment.Add(new StructureDepartment
                {
                    Name = "Институт информационных технологий",
                    ShortName = "ИИТ",
                    Type = "Факультет",
                    Parent = CSU
                });
                var ITEI = context.StructureDepartment.Add(new StructureDepartment
                {
                    Name = "Кафедра информационных технологий и экономической информатики",
                    ShortName = "Кафедра ИТиЭИ",
                    Type = "Кафедра",
                    Parent = IIT
                });
                /*context.StructureDepartment.Add(new StructureDepartment { Name = "ИЭКОБИА" });
                context.StructureDepartment.Add(new StructureDepartment { Name = "Биологический факультет" });*/

                /*Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\ИИТ");
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\ИЭКОБИА");
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\Биологический факультет");*/

                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"Rpds\ИИТ\Кафедра ИТЭИ");

                context.JobRole.Add(new JobRole { Name = "Преподаватель" });
                context.JobRole.Add(new JobRole { Name = "Методист" });
                context.JobRole.Add(new JobRole { Name = "Заведующий кафедрой" });
                context.SaveChanges();

                // создаем пользователей
                var admin = new ApplicationUser { Email = "admin@admin.com", UserName = "admin@admin.com", StructureDepartment = ITEI };
                string password = "Admin)0)";
                var result = userManager.Create(admin, password);

                // если создание пользователя прошло успешно
                if (result.Succeeded)
                {
                    // добавляем для пользователя роль
                    userManager.AddToRole(admin.Id, role1.Name);
                    userManager.AddToRole(admin.Id, role2.Name);
                }
            }
        }
    }
}
