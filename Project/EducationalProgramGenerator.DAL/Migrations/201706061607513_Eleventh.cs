namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eleventh : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanItems", "Zet", c => c.Int(nullable: false));
            AddColumn("dbo.PlanItems", "HoursInZet", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanItems", "HoursInZet");
            DropColumn("dbo.PlanItems", "Zet");
        }
    }
}
