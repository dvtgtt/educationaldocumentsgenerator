namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fifth : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Literatures", name: "Discipline_Id", newName: "KnowledgeItem_Id");
            RenameIndex(table: "dbo.Literatures", name: "IX_Discipline_Id", newName: "IX_KnowledgeItem_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Literatures", name: "IX_KnowledgeItem_Id", newName: "IX_Discipline_Id");
            RenameColumn(table: "dbo.Literatures", name: "KnowledgeItem_Id", newName: "Discipline_Id");
        }
    }
}
