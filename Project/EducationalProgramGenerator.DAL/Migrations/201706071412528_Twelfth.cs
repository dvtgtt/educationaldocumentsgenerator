namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Twelfth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanItems", "Gos", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanItems", "Gos");
        }
    }
}
