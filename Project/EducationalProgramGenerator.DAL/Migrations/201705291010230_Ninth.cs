namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ninth : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Discipline_Id", "dbo.Disciplines");
            DropIndex("dbo.AspNetUsers", new[] { "Discipline_Id" });
            CreateTable(
                "dbo.ApplicationUserDisciplines",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Discipline_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Discipline_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Discipline_Id);
            
            DropColumn("dbo.AspNetUsers", "Discipline_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Discipline_Id", c => c.Int());
            DropForeignKey("dbo.ApplicationUserDisciplines", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.ApplicationUserDisciplines", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ApplicationUserDisciplines", new[] { "Discipline_Id" });
            DropIndex("dbo.ApplicationUserDisciplines", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ApplicationUserDisciplines");
            CreateIndex("dbo.AspNetUsers", "Discipline_Id");
            AddForeignKey("dbo.AspNetUsers", "Discipline_Id", "dbo.Disciplines", "Id");
        }
    }
}
