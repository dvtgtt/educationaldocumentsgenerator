namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eighth3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ElectronicResources",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ModeOfAccess = c.String(),
                        CirculationDate = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.Id)
                .Index(t => t.Id);
            
            DropColumn("dbo.Literatures", "ModeOfAccess");
            DropColumn("dbo.Literatures", "CirculationDate");
            DropColumn("dbo.Literatures", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Literatures", "Discriminator", c => c.String(maxLength: 128));
            AddColumn("dbo.Literatures", "CirculationDate", c => c.String());
            AddColumn("dbo.Literatures", "ModeOfAccess", c => c.String());
            DropForeignKey("dbo.ElectronicResources", "Id", "dbo.Literatures");
            DropIndex("dbo.ElectronicResources", new[] { "Id" });
            DropTable("dbo.ElectronicResources");
        }
    }
}
