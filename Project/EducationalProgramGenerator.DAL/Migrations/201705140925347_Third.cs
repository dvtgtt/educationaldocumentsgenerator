namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Third : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StructureDepartments", "ShortName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StructureDepartments", "ShortName");
        }
    }
}
