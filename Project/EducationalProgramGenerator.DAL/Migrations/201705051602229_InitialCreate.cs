namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlanItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderNumber = c.Int(nullable: false),
                        Hours = c.Int(nullable: false),
                        Type = c.String(),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.KnowledgeItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        PlanItem_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.PlanItem_Id)
                .Index(t => t.PlanItem_Id);
            
            CreateTable(
                "dbo.LearningOutcomes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        LearningOutcomes_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LearningOutcomes", t => t.LearningOutcomes_Id)
                .Index(t => t.LearningOutcomes_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        StructureDepartmentId = c.Int(),
                        JobRoleId = c.Int(),
                        LastName = c.String(),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Discipline_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JobRoles", t => t.JobRoleId)
                .ForeignKey("dbo.StructureDepartments", t => t.StructureDepartmentId)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id)
                .Index(t => t.StructureDepartmentId)
                .Index(t => t.JobRoleId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Discipline_Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.JobRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.StructureDepartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StructureDepartments", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.Literatures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Section = c.String(),
                        Title1 = c.String(),
                        TitleInfo1 = c.String(),
                        EditionInfo = c.String(),
                        ParallelEditionInfo = c.String(),
                        AdditionalEditionInfoResponsible = c.String(),
                        Type = c.String(),
                        Size1 = c.String(),
                        ManufacturerPlace = c.String(),
                        ManufacturerName = c.String(),
                        ManufactureDate = c.String(),
                        PhysicalMediumInfo = c.String(),
                        OtherPhysicalMediumInfo = c.String(),
                        PhysicalMediumSize = c.String(),
                        AccompanyingMaterial = c.String(),
                        ModeOfAccess = c.String(),
                        CirculationDate = c.String(),
                        StandartNumber = c.String(),
                        StandartKeyTitle = c.String(),
                        StandartAccess = c.String(),
                        Discriminator = c.String(maxLength: 128),
                        Discipline_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id)
                .Index(t => t.Discipline_Id);
            
            CreateTable(
                "dbo.ElectronicResourceOutputs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EditionPlace = c.String(),
                        AndOtherEditionPlaces = c.Boolean(nullable: false),
                        AndOtherEditionNames = c.Boolean(nullable: false),
                        Date = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.ElectronicResource_Id)
                .Index(t => t.ElectronicResource_Id);
            
            CreateTable(
                "dbo.ElectronicResourceSeries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ISSN = c.String(),
                        Number = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.ElectronicResource_Id)
                .Index(t => t.ElectronicResource_Id);
            
            CreateTable(
                "dbo.ElectronicResourceSystemRequirements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ComputerNameAndModel = c.String(),
                        RAM = c.String(),
                        OS = c.String(),
                        Software = c.String(),
                        Peripherals = c.String(),
                        TechnicalMeans = c.String(),
                        ElectronicResource_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.ElectronicResource_Id)
                .Index(t => t.ElectronicResource_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.KnowledgeItemKnowledgeItems",
                c => new
                    {
                        KnowledgeItem_Id = c.Int(nullable: false),
                        KnowledgeItem_Id1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.KnowledgeItem_Id, t.KnowledgeItem_Id1 })
                .ForeignKey("dbo.KnowledgeItems", t => t.KnowledgeItem_Id)
                .ForeignKey("dbo.KnowledgeItems", t => t.KnowledgeItem_Id1)
                .Index(t => t.KnowledgeItem_Id)
                .Index(t => t.KnowledgeItem_Id1);
            
            CreateTable(
                "dbo.LearningOutcomesKnowledgeItems",
                c => new
                    {
                        LearningOutcomes_Id = c.Int(nullable: false),
                        KnowledgeItem_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LearningOutcomes_Id, t.KnowledgeItem_Id })
                .ForeignKey("dbo.LearningOutcomes", t => t.LearningOutcomes_Id, cascadeDelete: true)
                .ForeignKey("dbo.KnowledgeItems", t => t.KnowledgeItem_Id, cascadeDelete: true)
                .Index(t => t.LearningOutcomes_Id)
                .Index(t => t.KnowledgeItem_Id);
            
            CreateTable(
                "dbo.DisciplineDisciplines",
                c => new
                    {
                        Discipline_Id = c.Int(nullable: false),
                        Discipline_Id1 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Discipline_Id, t.Discipline_Id1 })
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id)
                .ForeignKey("dbo.Disciplines", t => t.Discipline_Id1)
                .Index(t => t.Discipline_Id)
                .Index(t => t.Discipline_Id1);
            
            CreateTable(
                "dbo.AcademicPlanItems",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        StructureDepartmentId = c.Int(nullable: false),
                        Direction = c.String(),
                        Qualification = c.String(),
                        ModeOfStudy = c.String(),
                        StartYear = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.Id)
                .ForeignKey("dbo.StructureDepartments", t => t.StructureDepartmentId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.StructureDepartmentId);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Author = c.String(),
                        Title = c.String(),
                        TitleInfo = c.String(),
                        Responsible = c.String(),
                        Reissue = c.String(),
                        City = c.String(),
                        PublishingHouse = c.String(),
                        Year = c.String(),
                        Size = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Disciplines",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Goals = c.String(),
                        Tasks = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.KnowledgeItems", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.SemesterItems",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Competencies",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LearningOutcomes", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.DisciplineLOs",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Competency_Id = c.Int(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LearningOutcomes", t => t.Id)
                .ForeignKey("dbo.Competencies", t => t.Competency_Id)
                .Index(t => t.Id)
                .Index(t => t.Competency_Id);
            
            CreateTable(
                "dbo.OtherLiterature",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Literatures", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Controls",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Controls", "Id", "dbo.PlanItems");
            DropForeignKey("dbo.OtherLiterature", "Id", "dbo.Literatures");
            DropForeignKey("dbo.DisciplineLOs", "Competency_Id", "dbo.Competencies");
            DropForeignKey("dbo.DisciplineLOs", "Id", "dbo.LearningOutcomes");
            DropForeignKey("dbo.Competencies", "Id", "dbo.LearningOutcomes");
            DropForeignKey("dbo.SemesterItems", "Id", "dbo.PlanItems");
            DropForeignKey("dbo.Disciplines", "Id", "dbo.KnowledgeItems");
            DropForeignKey("dbo.Books", "Id", "dbo.Literatures");
            DropForeignKey("dbo.AcademicPlanItems", "StructureDepartmentId", "dbo.StructureDepartments");
            DropForeignKey("dbo.AcademicPlanItems", "Id", "dbo.PlanItems");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.KnowledgeItems", "PlanItem_Id", "dbo.PlanItems");
            DropForeignKey("dbo.DisciplineDisciplines", "Discipline_Id1", "dbo.Disciplines");
            DropForeignKey("dbo.DisciplineDisciplines", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.Literatures", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.ElectronicResourceSystemRequirements", "ElectronicResource_Id", "dbo.Literatures");
            DropForeignKey("dbo.ElectronicResourceSeries", "ElectronicResource_Id", "dbo.Literatures");
            DropForeignKey("dbo.ElectronicResourceOutputs", "ElectronicResource_Id", "dbo.Literatures");
            DropForeignKey("dbo.AspNetUsers", "Discipline_Id", "dbo.Disciplines");
            DropForeignKey("dbo.AspNetUsers", "StructureDepartmentId", "dbo.StructureDepartments");
            DropForeignKey("dbo.StructureDepartments", "Parent_Id", "dbo.StructureDepartments");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "JobRoleId", "dbo.JobRoles");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LearningOutcomesKnowledgeItems", "KnowledgeItem_Id", "dbo.KnowledgeItems");
            DropForeignKey("dbo.LearningOutcomesKnowledgeItems", "LearningOutcomes_Id", "dbo.LearningOutcomes");
            DropForeignKey("dbo.LearningOutcomes", "LearningOutcomes_Id", "dbo.LearningOutcomes");
            DropForeignKey("dbo.KnowledgeItemKnowledgeItems", "KnowledgeItem_Id1", "dbo.KnowledgeItems");
            DropForeignKey("dbo.KnowledgeItemKnowledgeItems", "KnowledgeItem_Id", "dbo.KnowledgeItems");
            DropForeignKey("dbo.PlanItems", "Parent_Id", "dbo.PlanItems");
            DropIndex("dbo.Controls", new[] { "Id" });
            DropIndex("dbo.OtherLiterature", new[] { "Id" });
            DropIndex("dbo.DisciplineLOs", new[] { "Competency_Id" });
            DropIndex("dbo.DisciplineLOs", new[] { "Id" });
            DropIndex("dbo.Competencies", new[] { "Id" });
            DropIndex("dbo.SemesterItems", new[] { "Id" });
            DropIndex("dbo.Disciplines", new[] { "Id" });
            DropIndex("dbo.Books", new[] { "Id" });
            DropIndex("dbo.AcademicPlanItems", new[] { "StructureDepartmentId" });
            DropIndex("dbo.AcademicPlanItems", new[] { "Id" });
            DropIndex("dbo.DisciplineDisciplines", new[] { "Discipline_Id1" });
            DropIndex("dbo.DisciplineDisciplines", new[] { "Discipline_Id" });
            DropIndex("dbo.LearningOutcomesKnowledgeItems", new[] { "KnowledgeItem_Id" });
            DropIndex("dbo.LearningOutcomesKnowledgeItems", new[] { "LearningOutcomes_Id" });
            DropIndex("dbo.KnowledgeItemKnowledgeItems", new[] { "KnowledgeItem_Id1" });
            DropIndex("dbo.KnowledgeItemKnowledgeItems", new[] { "KnowledgeItem_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ElectronicResourceSystemRequirements", new[] { "ElectronicResource_Id" });
            DropIndex("dbo.ElectronicResourceSeries", new[] { "ElectronicResource_Id" });
            DropIndex("dbo.ElectronicResourceOutputs", new[] { "ElectronicResource_Id" });
            DropIndex("dbo.Literatures", new[] { "Discipline_Id" });
            DropIndex("dbo.StructureDepartments", new[] { "Parent_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Discipline_Id" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "JobRoleId" });
            DropIndex("dbo.AspNetUsers", new[] { "StructureDepartmentId" });
            DropIndex("dbo.LearningOutcomes", new[] { "LearningOutcomes_Id" });
            DropIndex("dbo.KnowledgeItems", new[] { "PlanItem_Id" });
            DropIndex("dbo.PlanItems", new[] { "Parent_Id" });
            DropTable("dbo.Controls");
            DropTable("dbo.OtherLiterature");
            DropTable("dbo.DisciplineLOs");
            DropTable("dbo.Competencies");
            DropTable("dbo.SemesterItems");
            DropTable("dbo.Disciplines");
            DropTable("dbo.Books");
            DropTable("dbo.AcademicPlanItems");
            DropTable("dbo.DisciplineDisciplines");
            DropTable("dbo.LearningOutcomesKnowledgeItems");
            DropTable("dbo.KnowledgeItemKnowledgeItems");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ElectronicResourceSystemRequirements");
            DropTable("dbo.ElectronicResourceSeries");
            DropTable("dbo.ElectronicResourceOutputs");
            DropTable("dbo.Literatures");
            DropTable("dbo.StructureDepartments");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.JobRoles");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.LearningOutcomes");
            DropTable("dbo.KnowledgeItems");
            DropTable("dbo.PlanItems");
        }
    }
}
