namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fourteenth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SemesterItems", "CourseWork", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SemesterItems", "CourseWork");
        }
    }
}
