namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fifteenth : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DisciplineItems",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        HoursInZet = c.Int(nullable: false),
                        Gos = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanItems", t => t.Id)
                .Index(t => t.Id);
            
            DropColumn("dbo.PlanItems", "HoursInZet");
            DropColumn("dbo.PlanItems", "Gos");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanItems", "Gos", c => c.Int(nullable: false));
            AddColumn("dbo.PlanItems", "HoursInZet", c => c.Int(nullable: false));
            DropForeignKey("dbo.DisciplineItems", "Id", "dbo.PlanItems");
            DropIndex("dbo.DisciplineItems", new[] { "Id" });
            DropTable("dbo.DisciplineItems");
        }
    }
}
