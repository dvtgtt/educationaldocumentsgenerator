namespace EducationalProgramGenerator.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tenth : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Disciplines", "Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Disciplines", "Code");
        }
    }
}
