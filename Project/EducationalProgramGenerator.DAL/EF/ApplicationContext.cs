﻿using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;

namespace EducationalProgramGenerator.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<StructureDepartment> StructureDepartment { get; set; }
        public DbSet<JobRole> JobRole { get; set; }
        public DbSet<PlanItem> PlanItem { get; set; }
        public DbSet<KnowledgeItem> KnowledgeItem { get; set; }
        public DbSet<LearningOutcomes> LearningOutcomes { get; set; }
        public DbSet<AcademicPlanItem> AcademicPlanItem { get; set; }
        public DbSet<SemesterItem> SemesterItem { get; set; }
        public DbSet<Discipline> Discipline { get; set; }
        public DbSet<Literature> Literature { get; set; }
        public DbSet<Book> Book { get; set; }

        //static ApplicationContext()
        //{
        //    Database.SetInitializer(new AppDbInitializer());
        //}

        public ApplicationContext()
        {

        }

        public ApplicationContext(string connection) : base(connection, throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>()
                        .HasOptional(p => p.StructureDepartment)
                        .WithMany(p => p.Users).
                        HasForeignKey(x => x.StructureDepartmentId);
            modelBuilder.Entity<ApplicationUser>()
                        .HasOptional(p => p.JobRole)
                        .WithMany(p => p.Users).
                        HasForeignKey(x => x.JobRoleId);
            base.OnModelCreating(modelBuilder);
        }
    }

    class AppDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationContext> //CreateDatabaseIfNotExists<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // создаем две роли
            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };
            var role3 = new IdentityRole { Name = "superuser" };

            // добавляем роли в бд
            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            var CSU = context.StructureDepartment.Add(new StructureDepartment {
                Name = "Челябинский государственный университет", Type = "Университет" });
            var chair = context.StructureDepartment.Add(new StructureDepartment {
                Name = "Кафедра информационных технологий и экономической информатики", Type = "Кафедра", Parent = CSU });
            var IIT = context.StructureDepartment.Add(new StructureDepartment {
                Name = "ИИТ", Type = "Факультет", Parent = chair });
            /*context.StructureDepartment.Add(new StructureDepartment { Name = "ИЭКОБИА" });
            context.StructureDepartment.Add(new StructureDepartment { Name = "Биологический факультет" });*/

            /*Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\ИИТ");
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\ИЭКОБИА");
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"AcademicPlans\Биологический факультет");*/

            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"Rpds\ИИТ");
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"Rpds\ИЭКОБИА");
            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"Rpds\Биологический факультет");

            context.JobRole.Add(new JobRole { Name = "Преподаватель" });
            context.JobRole.Add(new JobRole { Name = "Методист" });
            context.JobRole.Add(new JobRole { Name = "Заведующий кафедрой" });
            context.SaveChanges();

            // создаем пользователей
            var admin = new ApplicationUser { Email = "admin@admin.com", UserName = "admin@admin.com", StructureDepartment = IIT };
            string password = "Admin)0)";
            var result = userManager.Create(admin, password);

            // если создание пользователя прошло успешно
            if (result.Succeeded)
            {
                // добавляем для пользователя роль
                userManager.AddToRole(admin.Id, role1.Name);
                userManager.AddToRole(admin.Id, role2.Name);
            }

            base.Seed(context);
        }
    }
}
