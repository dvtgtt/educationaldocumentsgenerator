﻿using EducationalProgramGenerator.DAL.EF;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Repositories
{
    public class CRUDRepository : ICRUDRepository
    {
        private ApplicationContext db;

        public CRUDRepository(ApplicationContext context)
        {
            db = context;
        }

        public IQueryable<T> Select<T>()
            where T : class
        {
            return db.Set<T>();
        }

        public void Create<T>(T entity)
            where T : class
        {
            db.Entry(entity).State = EntityState.Added;
        }

        public T Get<T>(int id)
            where T : class
        {
            return db.Set<T>().Find(id);
        }

        public T Get<T>(string id)
            where T : class
        {
            return db.Set<T>().Find(id);
        }

        public void Update<T>(T entity)
            where T : class
        {
            db.Entry(entity).State = EntityState.Modified;
        }

        public void Delete<T>(T entity)
            where T : class
        {
            db.Entry(entity).State = EntityState.Deleted;
        }

        public void Attach<T>(T entity)
            where T : class
        {
            db.Entry(entity).State = EntityState.Unchanged;
        }

        public void SetValues<T>(T original, T entity)
            where T : class
        {
            db.Entry(original).CurrentValues.SetValues(entity);
        }
    }
}
