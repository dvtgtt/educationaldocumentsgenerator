﻿using EducationalProgramGenerator.DAL.EF;
using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Identity;
using EducationalProgramGenerator.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Repositories
{
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;
        
        private ApplicationUserManager userManager;
        private RoleManager<IdentityRole> roleManager;
        private ICRUDRepository _CRUDRepository;

        public IdentityUnitOfWork()
        {
#if DEBUG
            db = new ApplicationContext(@"Data Source=(LocalDb)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\aspnet-EducationalProgramGenerator.WEB-20170220075205.mdf;Initial Catalog=aspnet-EducationalProgramGenerator.WEB-20170220075205;Integrated Security=True");
#else
            db = new ApplicationContext(@"Data Source=WIN-6QRF800KKN1\SQLEXPRESS; Initial Catalog=OpenEducationDB; Persist Security Info=true; User ID=OpenEducation; Password=123456");
#endif
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            _CRUDRepository = new CRUDRepository(db);
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public RoleManager<IdentityRole> RoleManager
        {
            get { return roleManager; }
        }

        public ICRUDRepository CRUDRepository
        {
            get { return _CRUDRepository; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    db.Dispose();
                }
                disposed = true;
            }
        }
    }
}
