﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class StructureDepartment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Type { get; set; }
        public virtual List<StructureDepartment> Children { get; set; }
        public virtual StructureDepartment Parent { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }
}
