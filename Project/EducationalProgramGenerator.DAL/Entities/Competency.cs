﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("Competencies")]
    public class Competency : LearningOutcomes
    {
        public string Code { get; set; }
    }
}
