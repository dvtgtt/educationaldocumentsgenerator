﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("Books")]
    public class Book : Literature
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public string TitleInfo { get; set; }
        public string Responsible { get; set; }
        public string Reissue { get; set; }
        public string City { get; set; }
        public string PublishingHouse { get; set; }
        public string Year { get; set; }
        public string Size { get; set; }
    }
}
