﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class Literature
    {
        public int Id { get; set; }
        public string Section { get; set; }
        public virtual ICollection<KnowledgeItem> KnowledgeItems { get; set; }
    }
}
