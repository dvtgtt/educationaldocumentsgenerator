﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("ElectronicResources")]
    public class ElectronicResource : Literature
    {
        //Область заглавия и сведений об ответственности
        //Основное заглавие (после него '[Электронный ресурс]')
        public string Title { get; set; }
        //Параллельные заглавия (через '=')
        //public List<string> ParallelTitles { get; set; }
        //Сведения, относящиеся к заглавию (через ':')
        //public List<string> TitleInfo { get; set; }
        public string TitleInfo { get; set; }
        //Сведения об ответственности (через '/' первые, через ';' остальные (если они разнородные))
        //public List<string> Responsible { get; set; }


        ////Область издания
        ////Сведения об издании
        //public string EditionInfo { get; set; }
        ////Параллельные сведения об издании (через '=')
        //public string ParallelEditionInfo { get; set; }
        ////Сведения об издании (через '/' первые, через ';' остальные (если они разнородные))
        //public List<string> EditionInfoResponsible { get; set; }
        ////Дополнительные сведения (после основных через ',')
        //public string AdditionalEditionInfoResponsible { get; set; }


        ////Область вида и объема ресурса
        ////Обозначение вида ресурса
        //public string Type { get; set; }
        ////Объем ресурса
        //public string Size { get; set; }


        ////Область входных данных
        //public List<ElectronicResourceOutput> Output { get; set; }
        ////Далее в '()' (если нет выходных данных)
        ////Место изготовления
        //public string ManufacturerPlace { get; set; }
        ////Имя изготовителя (':')
        //public string ManufacturerName { get; set; }
        ////Дата изготовления(',')
        //public string ManufactureDate { get; set; }


        ////Область физической характеристики (для ресурсов локального доступа)
        ////Физический носитель (материал и количество)
        //public string PhysicalMediumInfo { get; set; }
        ////Другие физические характеристики (через ':')
        //public string OtherPhysicalMediumInfo { get; set; }
        ////Размер (через ';')
        //public string PhysicalMediumSize { get; set; }
        ////Сопроводительный материал (через '+')
        //public string AccompanyingMaterial { get; set; }


        ////Область серии
        ////В '()'
        //public List<ElectronicResourceSeries> Series { get; set; }


        ////Примечания
        ////Системные требования (для локального ресурса) 'Системные требования:'
        //public List<ElectronicResourceSystemRequirements> SystemRequirements { get; set; }
        //Режим доступа (для ресурса удаленного доступа) 'Режим доступа:'
        public string ModeOfAccess { get; set; }
        public string CirculationDate { get; set; } //Vottakvot
        //Другие примечания
        //public List<string> OtherNotes { get; set; }


        ////Область стандартного номера
        ////Стандартный номер
        //public string StandartNumber { get; set; }
        ////Ключевое заглавие (через '=')
        //public string StandartKeyTitle { get; set; }
        ////Условия доступности и (или) цена-а-а-а ааа ааа аааааа (через ':')
        //public string StandartAccess { get; set; }
    }

    //public class ElectronicResourceSystemRequirements
    //{
    //    public int Id { get; set; }
    //    //Все поля через ';'
    //    public string ComputerNameAndModel { get; set; }
    //    public string RAM { get; set; }
    //    public string OS { get; set; }
    //    public string Software { get; set; }
    //    public string Peripherals { get; set; }
    //    public string TechnicalMeans { get; set; }
    //}

    //public class ElectronicResourceSeries
    //{
    //    public int Id { get; set; }
    //    //как и для ресурса
    //    public string Title { get; set; }
    //    public List<string> ParallelTitles { get; set; }
    //    public List<string> TitleInfo { get; set; }
    //    public List<string> Responsible { get; set; }

    //    //Международный стандартный номер ISSN (через ',')
    //    public string ISSN { get; set; }
    //    //Нумерация внутри серии (через ';')
    //    public string Number { get; set; }
    //}

    //public class ElectronicResourceOutput
    //{
    //    public int Id { get; set; }
    //    //Места издания (через ';') (если ничего не указано - '[Б. м.]' или '[S. l.]' ааааааааа)
    //    public string EditionPlace { get; set; }
    //    //'[и др.]' или '[etc.]' бубухъ
    //    public bool AndOtherEditionPlaces { get; set; }
    //    //Имя издателя (через ':')
    //    public List<string> EditionNames { get; set; }
    //    //'[и др.]' или '[etc.]'
    //    public bool AndOtherEditionNames { get; set; }
    //    //Дата
    //    public string Date { get; set; }
    //}
}
