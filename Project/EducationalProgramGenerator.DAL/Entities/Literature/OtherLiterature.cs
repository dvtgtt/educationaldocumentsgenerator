﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("OtherLiterature")]
    public class OtherLiterature : Literature
    {
        public string Description { get; set; }
    }
}
