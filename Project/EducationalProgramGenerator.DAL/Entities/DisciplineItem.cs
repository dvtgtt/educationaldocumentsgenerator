﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("DisciplineItems")]
    public class DisciplineItem : PlanItem
    {
        public int HoursInZet { get; set; }
        public int Gos { get; set; }
    }
}
