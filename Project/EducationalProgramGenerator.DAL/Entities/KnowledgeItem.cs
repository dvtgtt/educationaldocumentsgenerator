﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class KnowledgeItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<KnowledgeItem> RequiredKnoledges { get; set; }
        public virtual List<KnowledgeItem> Children { get; set; }
        public virtual List<LearningOutcomes> Results { get; set; }
        public virtual List<Literature> Literature { get; set; }
    }
}
