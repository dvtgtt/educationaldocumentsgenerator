﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("SemesterItems")]
    public class SemesterItem : PlanItem
    {
        public int Exam { get; set; }
        public bool Pass { get; set; }
        public bool GradedTest { get; set; }
        public bool CourseWork { get; set; }
    }
}
