﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("AcademicPlanItems")]
    public class AcademicPlanItem : PlanItem
    {
        [ForeignKey("StructureDepartment")]
        public int StructureDepartmentId { get; set; }
        public virtual StructureDepartment StructureDepartment { get; set; }
        public string Direction { get; set; }
        public string Qualification { get; set; }
        public string ModeOfStudy { get; set; }
        public int StartYear { get; set; }
    }
}
