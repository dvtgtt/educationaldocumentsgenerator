﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class LearningOutcomes
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public virtual List<KnowledgeItem> Knowledges { get; set; }
        public virtual List<LearningOutcomes> Children { get; set; }
    }
}
