﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("DisciplineLOs")]
    public class DisciplineLO : LearningOutcomes
    {
        public string Type { get; set; }
        public virtual Competency Competency { get; set; }
    }
}
