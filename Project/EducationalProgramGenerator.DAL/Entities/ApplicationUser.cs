﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [ForeignKey("StructureDepartment")]
        public int? StructureDepartmentId { get; set; }
        public virtual StructureDepartment StructureDepartment { get; set; }

        [ForeignKey("JobRole")]
        public int? JobRoleId { get; set; }
        public virtual JobRole JobRole { get; set; }

        //[ForeignKey("AcademicDegree")]
        //public int? AcademicDegreeId { get; set; }
        //public virtual AcademicDegree AcademicDegree { get; set; }

        //[ForeignKey("AcademicRank")]
        //public int? AcademicRankId { get; set; }
        //public virtual AcademicRank AcademicRank { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public virtual ICollection<Discipline> Disciplines { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }
}
