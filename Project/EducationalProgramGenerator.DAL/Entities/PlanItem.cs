﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    public class PlanItem
    {
        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public int Hours { get; set; }
        public string Type { get; set; }
        public virtual List<PlanItem> Children { get; set; }
        public virtual List<KnowledgeItem> Knowledges { get; set; }
        public virtual PlanItem Parent { get; set; }
        public virtual Control Control { get; set; }
        public int Zet { get; set; }
    }
}
