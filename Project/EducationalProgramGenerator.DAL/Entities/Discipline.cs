﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Entities
{
    [Table("Disciplines")]
    public class Discipline : KnowledgeItem
    {
        public virtual List<ApplicationUser> Authors { get; set; }
        public string Goals { get; set; }
        public string Tasks { get; set; }
        public virtual List<Discipline> PreviousDisciplines { get; set; }
        public virtual List<Discipline> NextDisciplines { get; set; }
        public string Code { get; set; }
    }
}
