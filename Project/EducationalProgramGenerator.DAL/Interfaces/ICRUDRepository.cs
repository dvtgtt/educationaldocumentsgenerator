﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Interfaces
{
    public interface ICRUDRepository
    {
        IQueryable<T> Select<T>() where T : class;
        void Create<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        void Attach<T>(T entity) where T : class;
        T Get<T>(int id) where T : class;
        T Get<T>(string id) where T : class;
        void SetValues<T>(T original, T entity) where T : class;
    }
}