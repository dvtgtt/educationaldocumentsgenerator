﻿using EducationalProgramGenerator.DAL.Entities;
using EducationalProgramGenerator.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        RoleManager<IdentityRole> RoleManager { get; }
        ICRUDRepository CRUDRepository { get; }
        Task SaveAsync();
        void Save();
    }
}
