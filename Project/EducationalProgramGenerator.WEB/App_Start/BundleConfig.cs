﻿using System.Web;
using System.Web.Optimization;

namespace EducationalProgramGenerator.WEB
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/jquery.popup.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                        "~/Scripts/select2.full.min.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/app.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      //"~/Content/bootstrap.min2.css",
                      "~/Content/OpenEDU.css",
                      "~/Content/OpenEDU2.css",
                      "~/Content/skin-black-light.css",
                      "~/Content/popup.css",
                      "~/Content/select2.css"));

            bundles.Add(new ScriptBundle("~/bundles/handsontable").Include(
                      "~/Content/handsontable/dist/handsontable.full.min.js"));

            bundles.Add(new StyleBundle("~/Content/handsontable").Include(
                      "~/Content/handsontable/dist/handsontable.full.min.css"));
        }
    }
}
