﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using EducationalProgramGenerator.WEB.Models;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.BLL.Services;

namespace EducationalProgramGenerator.WEB
{
    public partial class Startup
    {
        IServiceCreator serviceCreator = new ServiceCreator();

        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext<IUserService>(serviceCreator.CreateUserService);
            app.CreatePerOwinContext<IJobRoleService>(serviceCreator.CreateJobRoleService);
            app.CreatePerOwinContext<IStructureDepartmentService>(serviceCreator.CreateStructureDepartmentService);
            app.CreatePerOwinContext<IRoleService>(serviceCreator.CreateRoleService);
            app.CreatePerOwinContext<IAcademicPlanService>(serviceCreator.CreateAcademicPlanService);
            app.CreatePerOwinContext<IDisciplineService>(serviceCreator.CreateDisciplineService);
            app.CreatePerOwinContext<IDocumentGeneratorService>(() => serviceCreator.CreateDocumentGenerator(HttpContext.Current.GetOwinContext().Get<IDisciplineService>()));
            app.CreatePerOwinContext<IRpdParserService>(serviceCreator.CreateRpdParserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);)
        }
    }
}