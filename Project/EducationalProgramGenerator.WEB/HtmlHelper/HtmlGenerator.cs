﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using EducationalProgramGenerator.BLL.Annotations;
using System.ComponentModel.DataAnnotations;
using EducationalProgramGenerator.BLL.DTO;

namespace EducationalProgramGenerator.WEB.HtmlHelper
{
    public static class HtmlGenerator
    {
        public static string GetSectionForm(PlanItemDTO discipline)
        {
            var tagInputGroup = new TagBuilder("div");
            tagInputGroup.MergeAttribute("class", "input-group col-xs-12");

            var sectionNamePart = new StringBuilder("<div class=\"col-md-8\"><label>Название раздела</label>" +
                "<input type=\"text\" class=\"form-control\" data-toggle=\"tooltip\" data-placement=\"right\"" +
                "title=\"\" placeholder=\"Введите название\" name=\"Название\"></div>").ToString();
            tagInputGroup.InnerHtml += sectionNamePart;

            var tagColSemestersList = new TagBuilder("div");
            tagColSemestersList.MergeAttribute("class", "col-md-4");
            var tagDivFGSemsestersList = new TagBuilder("div");
            tagDivFGSemsestersList.MergeAttribute("class", "form-group");
            var tagLabelSemestersList = new TagBuilder("label");
            tagLabelSemestersList.InnerHtml += "Семестр";
            var tagSelectSemestersList = new TagBuilder("select");
            tagSelectSemestersList.MergeAttribute("id", "select-popup");
            tagSelectSemestersList.MergeAttribute("class", "form-control");
            tagSelectSemestersList.MergeAttribute("style", "width: 100%;");
            tagSelectSemestersList.MergeAttribute("onchange", "getSemesterHours(this);");
            foreach (var sem in discipline.Children)
            {
                var tagOption = new TagBuilder("option");
                tagOption.InnerHtml += sem.OrderNumber;
                tagSelectSemestersList.InnerHtml += tagOption;
            }

            tagDivFGSemsestersList.InnerHtml += tagLabelSemestersList;
            tagDivFGSemsestersList.InnerHtml += tagSelectSemestersList;
            tagColSemestersList.InnerHtml += tagDivFGSemsestersList;
            tagInputGroup.InnerHtml += tagColSemestersList;

            var sectionHoursPart = new StringBuilder();
            var childrenHours = discipline.Children
                .First().Children
                .SelectMany(x => x.Children)
                .GroupBy(x => x.Parent.Type)
                .ToDictionary(x => x.Key, x => x.Select(h => h.Hours).Sum());
            sectionHoursPart.AppendFormat("<br>" +
                "<div class=\"col-md-4\"><label>Часов лекций</label>" +
                "<input type = \"text\" class=\"form-control\" data-toggle=\"tooltip\" data-placement=\"right\" " +
                    "title=\"\" placeholder=\"Осталось: {0} ч.\" id=\"section-lecture\" name=\"Лекции\"" +
                    "onkeypress=\"return isNumber(event);\"></div>" +
                    "<div class=\"col-md-4\"><label>Часов практик</label>" +
                "<input type = \"text\" class=\"form-control\" data-toggle=\"tooltip\" data-placement=\"right\" " +
                    "title=\"\" placeholder=\"Осталось: {1} ч.\" id=\"section-practice\" name=\"Практики\"" +
                    "onkeypress=\"return isNumber(event);\"></div>" +
                    "<div class=\"col-md-4\"><label>Часов СРС</label>" +
                    "<input type = \"text\" class=\"form-control\" data-toggle=\"tooltip\" data-placement=\"right\" " +
                    "title=\"\" placeholder=\"Осталось: {2} ч.\" id=\"section-srs\" name=\"СРС\"" +
                    "onkeypress=\"return isNumber(event);\"></div>",
                    discipline.Children.First().Children
                        .Where(x => x.Type == "Лекции").Select(x => x.Hours - 
                        (childrenHours.ContainsKey("Лекции") ? childrenHours["Лекции"] : 0)).FirstOrDefault(),
                    discipline.Children.First().Children
                        .Where(x => x.Type == "Практики").Select(x => x.Hours -
                        (childrenHours.ContainsKey("Практики") ? childrenHours["Практики"] : 0)).FirstOrDefault(),
                    discipline.Children.First().Children
                        .Where(x => x.Type == "СРС").Select(x => x.Hours -
                        (childrenHours.ContainsKey("СРС") ? childrenHours["СРС"] : 0)).FirstOrDefault()
                    );
            tagInputGroup.InnerHtml += sectionHoursPart.ToString();

            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            tagDiv.InnerHtml += tagInputGroup;
            tagDiv.InnerHtml += new TagBuilder("br");
            
            return tagDiv.ToString();
        }

        private static TagBuilder getDTOForm(TagBuilder tagContainer, Type type, object dto = null)
        {
            foreach (var p in type.GetProperties())
            {
                if (Attribute.IsDefined(p, typeof(HtmlAnnotationAttribute)))
                {
                    var attributeValue = Attribute.GetCustomAttribute(p, typeof(HtmlAnnotationAttribute)) as HtmlAnnotationAttribute;
                    //div row
                    var tagRow = new TagBuilder("div");
                    tagRow.MergeAttribute("class", "row");
                    if (!attributeValue.Visible)
                    {
                        tagRow.MergeAttribute("style", "display: none; margin-bottom: 5px;");
                    }
                    else
                    {
                        tagRow.MergeAttribute("style", "margin-bottom: 5px;");
                    }
                    //div first col
                    var tagCol1 = new TagBuilder("div");
                    tagCol1.MergeAttribute("class", "col-md-4");
                    //div label
                    var tagLabel = new TagBuilder("label");
                    if (attributeValue.Name != null)
                    {
                        tagLabel.InnerHtml += attributeValue.Name;
                    }
                    //div second col
                    var tagCol2 = new TagBuilder("div");
                    tagCol2.MergeAttribute("class", "col-md-8");
                    //div input
                    TagBuilder tagInput;
                    if (attributeValue.InputTag != null)
                        tagInput = new TagBuilder(attributeValue.InputTag);
                    else
                        tagInput = new TagBuilder("input");
                    tagInput.MergeAttribute("name", p.Name);
                    tagInput.MergeAttribute("class", "form-control");
                    if (attributeValue.Placeholder != null)
                    {
                        tagInput.MergeAttribute("placeholder", attributeValue.Placeholder);
                    }
                    if (attributeValue.IsNumber)
                    {
                        tagInput.MergeAttribute("onkeypress", "return isNumber(event);");
                    }
                    if (dto != null)
                    {
                        var value = dto.GetType().GetProperty(p.Name).GetValue(dto, null);
                        if (value != null)
                        {
                            tagInput.MergeAttribute("value", value.ToString());
                            if (attributeValue.InputTag != null)
                                tagInput.InnerHtml += value.ToString();
                        }
                    }
                    tagInput.MergeAttribute("onchange", "getResult();");

                    tagCol1.InnerHtml += tagLabel;
                    tagCol2.InnerHtml += tagInput;
                    tagRow.InnerHtml += tagCol1;
                    tagRow.InnerHtml += tagCol2;
                    tagContainer.InnerHtml += tagRow;
                }
            }
            return tagContainer;
        }

        private static void addErrorsTag(TagBuilder tagContainer)
        {
            var tagRowErrors = new TagBuilder("ul");
            tagRowErrors.MergeAttribute("id", "errors");
            tagRowErrors.MergeAttribute("class", "text-danger");
            tagContainer.InnerHtml += tagRowErrors;
        }

        public static string GetDTOForm(Type type, object dto = null)
        {
            if (dto != null)
            {
                type = dto.GetType();
            }
            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            //div container
            var tagContainer = new TagBuilder("div");
            tagContainer.MergeAttribute("class", "container");
            tagContainer = getDTOForm(tagContainer, type, dto);

            addErrorsTag(tagContainer);

            tagDiv.InnerHtml += tagContainer;
            return tagDiv.ToString();
        }



        public static string GetLectureForm(PlanItemDTO planItem = null)
        {
            KnowledgeItemDTO knowledgeItem = planItem != null ? planItem.Knowledges[0] : null;
            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            //div container
            var tagContainer = new TagBuilder("div");
            tagContainer.MergeAttribute("class", "container");
            tagContainer = getDTOForm(tagContainer, typeof(KnowledgeItemDTO), knowledgeItem);
            getDTOForm(tagContainer, typeof(PlanItemDTO), planItem);

            addErrorsTag(tagContainer);

            tagDiv.InnerHtml += tagContainer;
            return tagDiv.ToString();
        }

        public static string GetPracticeForm(List<LiteratureDTO> disciplineLiterature, List<string> GOSTLiterature, PlanItemDTO planItem = null)
        {
            KnowledgeItemDTO knowledgeItem = planItem != null ? planItem.Knowledges[0] : null;
            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            //div container
            var tagContainer = new TagBuilder("div");
            tagContainer.MergeAttribute("class", "container");
            tagContainer = getDTOForm(tagContainer, typeof(KnowledgeItemDTO), knowledgeItem);

            //LITERATUREEE
            var tagRow = new TagBuilder("div");
            tagRow.MergeAttribute("class", "row");
            tagRow.MergeAttribute("style", "margin-bottom: 5px;");
            var tagCol1 = new TagBuilder("div");
            tagCol1.MergeAttribute("class", "col-md-4");
            var tagLabel = new TagBuilder("label");
            tagLabel.InnerHtml += "Литература";
            var tagCol2 = new TagBuilder("div");
            tagCol2.MergeAttribute("class", "col-md-8");
            var tagSelect = new TagBuilder("select");
            tagSelect.MergeAttribute("name", "Literature");
            tagSelect.MergeAttribute("class", "form-control select2");
            tagSelect.MergeAttribute("required", "required");
            tagSelect.MergeAttribute("style", "width: 100%;");
            for (int i=0; i<disciplineLiterature.Count; i++)
            {
                var tagOption = new TagBuilder("option");
                tagOption.MergeAttribute("selected", planItem != null ? 
                    (planItem.Knowledges[0].Literature.Exists(x => x.Id == disciplineLiterature[i].Id) ? "true" : "false") : "false");
                tagOption.InnerHtml += GOSTLiterature[i];
                tagSelect.InnerHtml += tagOption;

                var tagTmp = new TagBuilder("div");
                tagTmp.MergeAttribute("style", "display:none;");
                tagTmp.MergeAttribute("id", disciplineLiterature[i].Id.ToString());
                tagTmp.MergeAttribute("kek", planItem != null ?
                    (planItem.Knowledges[0].Literature.Exists(x => x.Id == disciplineLiterature[i].Id) ? "kek" : "") : "");
                tagTmp.MergeAttribute("class", "hidden-options");
                tagTmp.InnerHtml += GOSTLiterature[i];
                tagCol2.InnerHtml += tagTmp;
            }
            tagCol1.InnerHtml += tagLabel;
            tagCol2.InnerHtml += tagSelect;
            tagRow.InnerHtml += tagCol1;
            tagRow.InnerHtml += tagCol2;
            tagContainer.InnerHtml += tagRow;
            //LITERATUREEE

            getDTOForm(tagContainer, typeof(PlanItemDTO), planItem);

            addErrorsTag(tagContainer);

            tagDiv.InnerHtml += tagContainer;
            tagDiv.InnerHtml += "<script type=\"text/javascript\">" +
                "$('.select2').select2({placeholder: \"Нажмите, чтобы выбрать\", multiple: true});" +
                (planItem == null ? "$('.select2').val('').select2();" : "var selected_options = []; " +
                "selected_options = $('.modal-body .hidden-options').filter(function () { return $(this).attr('kek') == 'kek'; })" +
                ".map(function () { return $(this).text().toString(); }).get(); " +
                "$('.select2').val(selected_options).select2();") +
                "</script>";
            return tagDiv.ToString();
        }

        public static string GetSRSForm(List<LiteratureDTO> disciplineLiterature, List<string> GOSTLiterature, List<ControlDTO> controls, PlanItemDTO planItem = null)
        {
            KnowledgeItemDTO knowledgeItem = planItem != null ? planItem.Knowledges[0] : null;
            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            //div container
            var tagContainer = new TagBuilder("div");
            tagContainer.MergeAttribute("class", "container");
            tagContainer = getDTOForm(tagContainer, typeof(KnowledgeItemDTO), knowledgeItem);

            //CONTROL$$$
            var tagRow = new TagBuilder("div");
            tagRow.MergeAttribute("class", "row");
            tagRow.MergeAttribute("style", "margin-bottom: 5px;");
            var tagCol1 = new TagBuilder("div");
            tagCol1.MergeAttribute("class", "col-md-4");
            var tagLabel = new TagBuilder("label");
            tagLabel.InnerHtml += "Форма контроля";
            var tagCol2 = new TagBuilder("div");
            tagCol2.MergeAttribute("class", "col-md-8");
            var tagSelect = new TagBuilder("select");
            tagSelect.MergeAttribute("name", "Control");
            tagSelect.MergeAttribute("class", "form-control select2");
            tagSelect.MergeAttribute("required", "required");
            tagSelect.MergeAttribute("style", "width: 100%;");
            foreach (var item in controls)
            {
                var tagOption = new TagBuilder("option");
                tagOption.MergeAttribute("selected", planItem != null ? (item.Name == planItem.Control.Name ? "true" : "false") : "false");
                tagOption.InnerHtml += item.Name;
                tagSelect.InnerHtml += tagOption;
            }
            tagCol1.InnerHtml += tagLabel;
            tagCol2.InnerHtml += tagSelect;
            tagRow.InnerHtml += tagCol1;
            tagRow.InnerHtml += tagCol2;
            tagContainer.InnerHtml += tagRow;
            //ENDOFCONTROL$$$

            //LITERATUREEE
            tagRow = new TagBuilder("div");
            tagRow.MergeAttribute("class", "row");
            tagRow.MergeAttribute("style", "margin-bottom: 5px;");
            tagCol1 = new TagBuilder("div");
            tagCol1.MergeAttribute("class", "col-md-4");
            tagLabel = new TagBuilder("label");
            tagLabel.InnerHtml += "Литература";
            tagCol2 = new TagBuilder("div");
            tagCol2.MergeAttribute("class", "col-md-8");
            tagSelect = new TagBuilder("select");
            tagSelect.MergeAttribute("name", "Literature");
            tagSelect.MergeAttribute("class", "form-control select2");
            tagSelect.MergeAttribute("required", "required");
            tagSelect.MergeAttribute("style", "width: 100%;");
            for (int i = 0; i < disciplineLiterature.Count; i++)
            {
                var tagOption = new TagBuilder("option");
                tagOption.MergeAttribute("selected", planItem != null ?
                    (planItem.Knowledges[0].Literature.Exists(x => x.Id == disciplineLiterature[i].Id) ? "true" : "false") : "false");
                tagOption.InnerHtml += GOSTLiterature[i];
                tagSelect.InnerHtml += tagOption;

                var tagTmp = new TagBuilder("div");
                tagTmp.MergeAttribute("style", "display:none;");
                tagTmp.MergeAttribute("id", disciplineLiterature[i].Id.ToString());
                tagTmp.MergeAttribute("kek", planItem != null ?
                    (planItem.Knowledges[0].Literature.Exists(x => x.Id == disciplineLiterature[i].Id) ? "kek" : "") : "");
                tagTmp.MergeAttribute("class", "hidden-options");
                tagTmp.InnerHtml += GOSTLiterature[i];
                tagCol2.InnerHtml += tagTmp;
            }
            tagCol1.InnerHtml += tagLabel;
            tagCol2.InnerHtml += tagSelect;
            tagRow.InnerHtml += tagCol1;
            tagRow.InnerHtml += tagCol2;
            tagContainer.InnerHtml += tagRow;
            //LITERATUREEE

            getDTOForm(tagContainer, typeof(PlanItemDTO), planItem);

            addErrorsTag(tagContainer);

            tagDiv.InnerHtml += tagContainer;
            tagDiv.InnerHtml += "<script type=\"text/javascript\">" +
                "$('.select2[name=\"Literature\"]').select2({placeholder: \"Нажмите, чтобы выбрать\", multiple: true});" +
                (planItem == null ? "$('.select2[name=\"Literature\"]').val('').select2();" : "var selected_options = []; " +
                "selected_options = $('.modal-body .hidden-options').filter(function () { return $(this).attr('kek') == 'kek'; })" +
                ".map(function () { return $(this).text().toString(); }).get(); " +
                "$('.select2[name=\"Literature\"]').val(selected_options).select2();") +
                (planItem == null ? "$('.select2[name=\"Control\"]').val('');" : string.Format("$('.select2[name=\"Control\"]').val('{0}');", planItem.Control.Name)) +
                "$('.select2[name=\"Control\"]').select2({placeholder: \"Нажмите, чтобы выбрать\", tags:true, multiple: true, maximumSelectionLength: 1});" +
                "</script>";
            return tagDiv.ToString();
        }

        public static string GetDTOFormWithResult(Type type, object dto = null)
        {
            if (dto != null)
            {
                type = dto.GetType();
            }
            var tagDiv = new TagBuilder("div");
            tagDiv.MergeAttribute("class", "form-group");
            //div container
            var tagContainer = new TagBuilder("div");
            tagContainer.MergeAttribute("class", "container");
            var tagMainRow = new TagBuilder("div");
            tagMainRow.MergeAttribute("class", "row");
            var tagInputsCol = new TagBuilder("div");
            tagInputsCol.MergeAttribute("class", "col-md-6");
            tagInputsCol = getDTOForm(tagInputsCol, type, dto);

            var tagRowResult = new TagBuilder("div");
            var tagDivResult1 = new TagBuilder("div");
            var tagDivResult2 = new TagBuilder("div");
            tagDivResult1.InnerHtml += "Результат: ";
            tagDivResult2.MergeAttribute("id", "result");
            tagRowResult.MergeAttribute("class", "row");
            tagRowResult.InnerHtml += tagDivResult1;
            tagRowResult.InnerHtml += tagDivResult2;

            var tagResultCol = new TagBuilder("div");
            tagResultCol.MergeAttribute("class", "col-md-6");
            tagResultCol.InnerHtml += tagRowResult;

            tagMainRow.InnerHtml += tagInputsCol;
            tagMainRow.InnerHtml += tagResultCol;
            tagContainer.InnerHtml += tagMainRow;

            addErrorsTag(tagContainer);

            tagDiv.InnerHtml += tagContainer;
            return tagDiv.ToString();
        }
    }
}
