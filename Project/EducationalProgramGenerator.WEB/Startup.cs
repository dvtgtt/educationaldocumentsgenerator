﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EducationalProgramGenerator.WEB.Startup))]
namespace EducationalProgramGenerator.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
