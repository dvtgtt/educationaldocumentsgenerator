﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.WEB.Models
{
    public class EditDisciplineViewModel
    {

        [Required]
        [Display(Name = "Автор программы")]
        public string Author { get; set; }

        [Required]
        [Display(Name = "Цели изучения дисциплины")]
        public string Goals { get; set; }

        [Required]
        [Display(Name = "Задачи изучения дисциплины")]
        public string Tasks { get; set; }

        [Required]
        [Display(Name = "Перечень планируемых результатов обучения")]
        public List<CompetencyViewModel> Competencies { get; set; }

        [Display(Name = "Предшествующие дисциплины")]
        public string Previous { get; set; }

        [Display(Name = "Последующие дисциплины")]
        public string Next { get; set; }

        [Required]
        [Display(Name = "Типы занятий")]
        public List<Section> Semesters { get; set; }
    }

    public class Section
    {
        [Required]
        [Display(Name = "Номер")]
        public string Number { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        public List<double> TypesOfClasses { get; set; }
    }

    public class CompetencyViewModel
    {
        public CompetencyViewModel()
        {

        }
        [Required]
        [Display(Name = "Знать")]
        public string ToKnow { get; set; }

        [Required]
        [Display(Name = "Уметь")]
        public string ToCan { get; set; }

        [Required]
        [Display(Name = "Владеть")]
        public string ToOwn { get; set; }
    }
}
