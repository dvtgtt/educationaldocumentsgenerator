﻿using EducationalProgramGenerator.BLL.Interfaces;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EducationalProgramGenerator.WEB.Controllers
{
    public class BaseController : Controller
    {
        public IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public IStructureDepartmentService StructureDepartmentService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IStructureDepartmentService>();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserService.Dispose();
                StructureDepartmentService.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}