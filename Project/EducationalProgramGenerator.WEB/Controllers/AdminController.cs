﻿using EducationalProgramGenerator.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Specialized;
using EducationalProgramGenerator.BLL.DTO;
using System.Threading.Tasks;

namespace EducationalProgramGenerator.WEB.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : BaseController
    {
        private IJobRoleService JobRoleService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IJobRoleService>();
            }
        }

        private IRoleService RoleService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IRoleService>();
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult JobRoles()
        {
            NameValueCollection nvc = Request.Form;
            if (!string.IsNullOrEmpty(nvc["IdToChange"]))
            {
                JobRoleService.Update(new JobRoleDTO { Id = int.Parse(nvc["IdToChange"]), Name = nvc["NameToChange"] });
            }
            else if(!string.IsNullOrEmpty(nvc["IdToDelete"]))
            {
                JobRoleService.Delete(int.Parse(nvc["IdToDelete"]));
            }
            else if (!string.IsNullOrEmpty(nvc["NameToCreate"]))
            {
                JobRoleService.Create(new JobRoleDTO { Name = nvc["NameToCreate"] });
            }
            ViewBag.JobRoles = JobRoleService.GetAll();
            return View();
        }

        public ActionResult StructureDepartments()
        {
            NameValueCollection nvc = Request.Form;
            if (!string.IsNullOrEmpty(nvc["IdToChange"]))
            {
                StructureDepartmentService.Update(new StructureDepartmentDTO { Id = int.Parse(nvc["IdToChange"]), Name = nvc["NameToChange"] });
            }
            else if (!string.IsNullOrEmpty(nvc["IdToDelete"]))
            {
                StructureDepartmentService.Delete(int.Parse(nvc["IdToDelete"]));
            }
            else if (!string.IsNullOrEmpty(nvc["NameToCreate"]))
            {
                StructureDepartmentService.Create(new StructureDepartmentDTO { Name = nvc["NameToCreate"] });
            }
            ViewBag.StructureDepartments = StructureDepartmentService.GetAll();
            return View();
        }

        public async Task<ActionResult> Roles()
        {
            NameValueCollection nvc = Request.Form;
            if (!string.IsNullOrEmpty(nvc["NewName"]))
            {
                await RoleService.UpdateAsync(nvc["OldName"], nvc["NewName"]);
            }
            else if (!string.IsNullOrEmpty(nvc["NameToDelete"]))
            {
                await RoleService.DeleteAsync(nvc["NameToDelete"]);
            }
            else if (!string.IsNullOrEmpty(nvc["NameToCreate"]))
            {
                await RoleService.CreateAsync(nvc["NameToCreate"]);
            }
            ViewBag.Roles = RoleService.GetAll();
            return View();
        }

        public async Task<ActionResult> Users()
        {
            NameValueCollection nvc = Request.Form;
            if (!string.IsNullOrEmpty(nvc["Email"]))
            {
                await UserService.DeleteAsync(nvc["Email"]);
            }
            ViewBag.Users = UserService.GetAll();
            return View();
        }
    }
}