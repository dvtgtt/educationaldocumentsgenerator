﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using EducationalProgramGenerator.WEB.Models;
using EducationalProgramGenerator.BLL.Interfaces;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Routing;
using EducationalProgramGenerator.BLL.DTO;
using Newtonsoft.Json;
using EducationalProgramGenerator.WEB.HtmlHelper;
using System.Web.Script.Serialization;
using System.Reflection;
using EducationalProgramGenerator.BLL.Services;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;

namespace EducationalProgramGenerator.WEB.Controllers
{
    [Authorize]
    public class RpdGeneratorController : BaseController
    {
        private IAcademicPlanService AcademicPlanService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IAcademicPlanService>();
            }
        }

        private IDocumentGeneratorService DocumentGeneratorService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IDocumentGeneratorService>();
            }
        }

        private IDisciplineService DisciplineService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IDisciplineService>();
            }
        }

        private IRpdParserService RpdParserService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IRpdParserService>();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var userFromAuthCookie = System.Threading.Thread.CurrentPrincipal;

            var user = UserService.FindByUserName(userFromAuthCookie.Identity.Name);
            if (user.StructureDepartmentId == 0)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Message",
                    message = "Сначала установите структурное подразделение"
                }));
            }
        }

        public ActionResult Index()
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plans = AcademicPlanService.GetAcademicPlansNames(structureDepartmentId);
            return View();
        }

        public ActionResult DisciplineInformation(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;

            var authors = UserService.GetAll();
            ViewBag.Authors = authors;
            var currentAuthors = DisciplineService.GetAuthors(structureDepartmentId, plan, discipline);
            ViewBag.SelectedAuthors =
                authors.Select(item => currentAuthors.Exists(x => x.Id == item.Id)).ToList();

            ViewBag.Goals = DisciplineService.GetGoals(structureDepartmentId, discipline, plan);
            ViewBag.Tasks = DisciplineService.GetTasks(structureDepartmentId, discipline, plan);

            ViewBag.StartYear = AcademicPlanService.GetStartYear(structureDepartmentId, plan);

            var previousDisciplines = AcademicPlanService.GetPreviousDisciplinesNames(structureDepartmentId, plan, discipline);
            ViewBag.PreviousDisciplines = previousDisciplines;
            var currentPreviousDisciplines = DisciplineService.GetPreviousDisciplines(structureDepartmentId, discipline, plan);
            ViewBag.SelectedPreviousDisciplines =
                previousDisciplines.Select(item => currentPreviousDisciplines.Contains(item)).ToList();

            var nextDisciplines = AcademicPlanService.GetNextDisciplinesNames(structureDepartmentId, plan, discipline);
            ViewBag.NextDisciplines = nextDisciplines;
            var currentNextDisciplines = DisciplineService.GetNextDisciplines(structureDepartmentId, discipline, plan);
            ViewBag.SelectedNextDisciplines =
                nextDisciplines.Select(item => currentNextDisciplines.Contains(item)).ToList();

            return View();
        }

        [HttpPost]
        public ActionResult Index(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;

            ViewBag.StartYear = AcademicPlanService.GetStartYear(structureDepartmentId, plan);

            return View("Menu");   
        }

        public ActionResult Menu(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;

            ViewBag.StartYear = AcademicPlanService.GetStartYear(structureDepartmentId, plan);

            return View();
        }

        [HttpPost]
        public ActionResult EditDiscipline(EditDisciplineViewModel model, string plan, string discipline)
        {

            var dto = new DisciplineDTO(discipline, model.Goals+"\n"+model.Tasks);
            //dto.AuthorId = model.Author;

            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var competencies = AcademicPlanService.GetDisciplineCompetencies(structureDepartmentId, plan, discipline);

            for (int i = 0; i < model.Competencies.Count; ++i)
            {
                var competency = new CompetencyDTO(competencies[i].Description, competencies[i].Code);
                competency.Children.Add(new LearningOutcomesDTO(model.Competencies[i].ToKnow));
                competency.Children.Add(new LearningOutcomesDTO(model.Competencies[i].ToCan));
                competency.Children.Add(new LearningOutcomesDTO(model.Competencies[i].ToOwn));
                dto.Results.Add(competency);
            }


            //ViewBag.Url = DocumentGeneratorService.CreateDocument(plan, discipline, dto);
            return View("DownloadDocument");
        }

        #region Информация о дисциплине
        public JsonResult AddGoals(string goals, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.AddGoals(structureDepartmentId, plan, discipline, goals);
            return Json(new { data = "success" });
        }

        public JsonResult AddTasks(string tasks, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.AddTasks(structureDepartmentId, plan, discipline, tasks);
            return Json(new { data = "success" });
        }

        public JsonResult AddPreviousDiscipline(string previous, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.AddPreviousDiscipline(structureDepartmentId, plan, discipline, previous);
            return Json(new { data = "success" });
        }

        public JsonResult RemovePreviousDiscipline(string previous, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.RemovePreviousDiscipline(structureDepartmentId, plan, discipline, previous);
            return Json(new { data = "success" });
        }

        public JsonResult AddNextDiscipline(string next, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.AddNextDiscipline(structureDepartmentId, plan, discipline, next);
            return Json(new { data = "success" });
        }

        public JsonResult RemoveNextDiscipline(string next, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.RemoveNextDiscipline(structureDepartmentId, plan, discipline, next);
            return Json(new { data = "success" });
        }

        public JsonResult AddDisciplineAuthor(string id, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.AddAuthor(structureDepartmentId, plan, discipline, id);
            return Json(new { data = "success" });
        }

        public JsonResult RemoveDisciplineAuthor(string id, string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.RemoveAuthor(structureDepartmentId, plan, discipline, id);
            return Json(new { data = "success" });
        }
        #endregion

        #region Структура дисциплины
        public ActionResult EditStructure(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            ViewBag.StartYear = AcademicPlanService.GetStartYear(structureDepartmentId, plan);
            ViewBag.Semesters = AcademicPlanService.GetDiscipline(structureDepartmentId, plan, discipline)
                .Children.Select(x => x.OrderNumber);
            return View();
        }

        public PartialViewResult GetPlanItems(string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var disciplineDTO = AcademicPlanService.GetDiscipline(structureDepartmentId, plan, discipline);
            return PartialView(disciplineDTO);
        }

        public PartialViewResult GetDisciplines(string plan)
        {
            plan = HttpUtility.UrlDecode(plan);
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var disciplines = AcademicPlanService.GetDisciplinesNames(structureDepartmentId, plan);
            return PartialView(disciplines);
        }

        public string GetPlanItemCard(string plan, string discipline, string type, int id = 0)
        {
            PlanItemDTO planItem;
            if (id > 0)
                planItem = DisciplineService.GetPlanItem(id);
            else
                planItem = null;
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            switch (type)
            {
                case "Лекция":
                    return HtmlGenerator.GetLectureForm(planItem);
                case "Практика":
                    {
                        var disciplineLiterature = DisciplineService.GetLiterature(structureDepartmentId, plan, discipline);
                        var GOSTLiterature = new List<string>();
                        foreach (var item in disciplineLiterature)
                            GOSTLiterature.Add(GetGOSTTemplateEngine(item.GetType()).getGOSTString(item));
                        return HtmlGenerator.GetPracticeForm(disciplineLiterature, GOSTLiterature, planItem);
                    }
                case "СРС":
                    {
                        var disciplineLiterature = DisciplineService.GetLiterature(structureDepartmentId, plan, discipline);
                        var GOSTLiterature = new List<string>();
                        foreach (var item in disciplineLiterature)
                            GOSTLiterature.Add(GetGOSTTemplateEngine(item.GetType()).getGOSTString(item));
                        var controls = DisciplineService.GetControls();
                        return HtmlGenerator.GetSRSForm(disciplineLiterature, GOSTLiterature, controls, planItem);
                    }
                default:
                    return "(:";
            }
        }

        public JsonResult SavePlanItem(int sectionId, string type, Dictionary<string, string> inputs,
            List<int> literatureIds, string control)
        {
            var knowledgeItem = new KnowledgeItemDTO(inputs["Name"], inputs["Description"]);
            var planItem = new PlanItemDTO(type, int.Parse(inputs["Hours"]));
            
            planItem.Knowledges.Add(knowledgeItem);

            int id;
            if (int.TryParse(inputs["Id"], out id))
            {
                planItem.Id = id;
                planItem.OrderNumber = int.Parse(inputs["OrderNumber"]);
            }
            else planItem.Id = 0;
            switch (type)
            {
                case "Лекция":
                    DisciplineService.SavePlanItem(planItem, sectionId);
                    break;
                case "Практика":
                    foreach (var item in literatureIds)
                    {
                        planItem.Knowledges[0].Literature.Add(new LiteratureDTO { Id = item });
                    }
                    DisciplineService.SavePlanItem(planItem, sectionId);
                    break;
                case "СРС":
                    foreach (var item in literatureIds)
                    {
                        planItem.Knowledges[0].Literature.Add(new LiteratureDTO { Id = item });
                    }
                    planItem.Control = new ControlDTO { Name = control };
                    DisciplineService.SavePlanItem(planItem, sectionId);
                    break;
                default:
                    return Json(new { data = "(:" });
            }
            
            return Json(new { data = "success" });
        }

        public JsonResult RemovePlanItem(int id)
        {
            DisciplineService.RemovePlanItem(id);
            return Json(new { data = "success" });
        }

        public JsonResult ChangeOrder(int[] order)
        {
            for (int i = 0; i < order.Length; ++i)
            {
                var planItem = DisciplineService.GetPlanItem(order[i]);
                if (planItem != null)
                {
                    planItem.OrderNumber = i;
                    DisciplineService.SavePlanItem(planItem);
                }
            }
            return Json(new { data = "success" });
        }

        public JsonResult GetCurrentHours()
        {
            return Json(new { data = "success", hours = 0 });//DisciplineService.GetCurrentHours() });
        }
        #endregion

        #region Компетенции
        public ActionResult EditCompetencies(string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            ViewBag.StartYear = AcademicPlanService.GetStartYear(structureDepartmentId, plan);

            ViewBag.CompetenciesCodes = AcademicPlanService.GetDisciplineCompetenciesCodes(structureDepartmentId, plan, discipline);
            ViewBag.CompetenciesDescriptions = AcademicPlanService.GetDisciplineCompetenciesDescriptions(structureDepartmentId, plan, discipline);
            return View();
        }

        public PartialViewResult GetLearningOutcomes(string discipline, string plan, string type, string competencyCode)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            dynamic model = new ExpandoObject();
            model.Type = type;
            model.Items = new List<Dictionary<string, string>>();
            model.CompetencyCode = competencyCode;
            foreach (var item in DisciplineService.GetDisciplineLOs(structureDepartmentId, discipline, plan, type, competencyCode))
            {
                var modelItem = new Dictionary<string, string>();
                
                modelItem.Add("id", item.Id.ToString());
                modelItem.Add("description", item.Description);

                model.Items.Add(modelItem);
            }
            return PartialView(model);
        }

        public string GetLearningOutcomesCard(int id = 0)
        {
            if (id > 0)
            {
                var learningOutcomes = DisciplineService.GetDisciplineLO(id);
                return HtmlGenerator.GetDTOForm(typeof(DisciplineLODTO), learningOutcomes);
            }
            return HtmlGenerator.GetDTOForm(typeof(DisciplineLODTO));
        }

        public JsonResult SaveLearningOutcomes(string discipline, string plan, string competency, Dictionary<string, string> inputs)
        {
            var disciplineLO = new DisciplineLODTO(inputs["Description"]);
            disciplineLO.Type = inputs["Type"];

            int id;
            if (int.TryParse(inputs["Id"], out id))
            {
                disciplineLO.Id = id;
            }
            else disciplineLO.Id = 0;

            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.SaveDisciplineLO(structureDepartmentId, plan, discipline, competency, disciplineLO);
            return Json(new { data = "success" });
        }

        public JsonResult RemoveLearningOutcomes(int id)
        {
            DisciplineService.RemoveLearningOutcomes(id);
            return Json(new { data = "success" });
        }

        public string GetSectionCard(string discipline, string plan, int id = 0)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var disciplineDTO = AcademicPlanService.GetDiscipline(structureDepartmentId, plan, discipline);
            return HtmlGenerator.GetSectionForm(disciplineDTO);
        }

        public JsonResult GetSemesterHours(string discipline, string plan, int semesterNumber)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var remainingHours = DisciplineService.GetRemainingHours(structureDepartmentId, plan, discipline, semesterNumber);
            return Json(new { data = "success", remainingHours = remainingHours });
        }

        public JsonResult SaveSection(string discipline, string plan, Dictionary<string, string> inputs, int semesterNumber)
        {
            int lectureHours = int.TryParse(inputs["Лекции"], out lectureHours) ? lectureHours : 0;
            var lectureDTO = new PlanItemDTO("Раздел", lectureHours);
            int practiceHours = int.TryParse(inputs["Практики"], out practiceHours) ? practiceHours : 0;
            var practiceDTO = new PlanItemDTO("Раздел", practiceHours);
            int srsHours = int.TryParse(inputs["СРС"], out srsHours) ? srsHours : 0;
            var srsDTO = new PlanItemDTO("Раздел", srsHours);
            var sectionDTO = new KnowledgeItemDTO(inputs["Название"]);

            lectureDTO.Knowledges.Add(sectionDTO);
            practiceDTO.Knowledges.Add(sectionDTO);
            srsDTO.Knowledges.Add(sectionDTO);

            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            DisciplineService.SaveSection(structureDepartmentId, plan, discipline, semesterNumber,
                lectureDTO, practiceDTO, srsDTO, sectionDTO);

            return Json(new { data = "success" });
        }

        public JsonResult AddDisciplineLOToSection(int disciplineLOId, int sectionId)
        {
            DisciplineService.AddDisciplineLOToSection(disciplineLOId, sectionId);
            return Json(new { data = "success" });
        }

        public JsonResult RemoveDisciplineLOFromSection(int disciplineLOId, int sectionId)
        {
            DisciplineService.RemoveDisciplineLOFromSection(disciplineLOId, sectionId);
            return Json(new { data = "success" });
        }

        #endregion

        #region Литература
        public ActionResult EditLiterature(string discipline, string plan)
        {
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            return View();
        }

        public PartialViewResult GetDisciplineLiterature(string plan, string discipline)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var literature = AcademicPlanService.GetDisciplineLiterature(structureDepartmentId, plan, discipline);
            return PartialView(literature);
        }

        public JsonResult GetLiteratureResult(int type, Dictionary<string, string> inputs)
        { 
            var serializer = new JavaScriptSerializer();
            int id;
            Type detectedType;
            if (int.TryParse(inputs["Id"], out id))
            {
                detectedType = DisciplineService.GetLiteratureItem(id).GetType();
            }
            else
            {
                detectedType = DetectLiteratureType(type);
                inputs["Id"] = "0";
            }
            LiteratureDTO dto = serializer.Deserialize(serializer.Serialize(inputs), detectedType) as LiteratureDTO;
            var GOSTTemplateEngine = GetGOSTTemplateEngine(detectedType);
            string result = GOSTTemplateEngine.getGOSTString(dto);
            return Json(new { data = "success", result = result });
        }

        private Type DetectLiteratureType(int type)
        {
            switch (type)
            {
                case 1:
                    return typeof(BookDTO);
                case 2:
                    return typeof(ElectronicResourceDTO);
                case 3:
                    return typeof(OtherLiteratureDTO);
                default:
                    throw new NotImplementedException();
            }
        }

        private IGOSTTemplateEngine GetGOSTTemplateEngine(Type literatureType)
        {
            switch (literatureType.Name)
            {
                case "BookDTO":
                    return new BookGOSTTemplateEngine();
                case "ElectronicResourceDTO":
                    return new ElectronicResourceGOSTTemplateEngine();
                case "OtherLiteratureDTO":
                    return new OtherLiteratureGOSTTemplateEngine();
                default:
                    throw new NotImplementedException();
            }
        }

        public string GetLiteratureItem(int type = 0, int id = 0)
        {
            if (type != 0)
            {
                var detectedType = DetectLiteratureType(type);
                return HtmlGenerator.GetDTOFormWithResult(detectedType);
            }
            else if (id != 0)
            {
                var literature = DisciplineService.GetLiteratureItem(id);
                return HtmlGenerator.GetDTOFormWithResult(null, literature).ToString();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public JsonResult SaveLiteratureItem(string plan, string discipline, string section, int type, Dictionary<string, string> inputs)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var serializer = new JavaScriptSerializer();
            int id;
            Type detectedType;
            if (int.TryParse(inputs["Id"], out id))
            {
                detectedType = DisciplineService.GetLiteratureItem(id).GetType();
            }
            else
            {
                detectedType = DetectLiteratureType(type);
                inputs["Id"] = "0";
            }
            LiteratureDTO dto = serializer.Deserialize(serializer.Serialize(inputs), detectedType) as LiteratureDTO;
            dto.Section = section;
            var validateResults = DTOValidator.DTOValidator.TryValidateObject(dto);
            if (validateResults.Count != 0)
            {
                return Json(new { errors = validateResults });
            }
            DisciplineService.SaveLiteratureItem(structureDepartmentId, plan, discipline, dto);
            return Json(new { data = "success" });
        }

        public JsonResult RemoveLiteratureItem(int id)
        {
            DisciplineService.RemoveLiteratureItem(id);
            return Json(new { data = "success" });
        }
        #endregion

        #region Копирование
        public ActionResult GetUserRPDs()
        {
            ViewBag.RPDs = DisciplineService.GetUserRPDs(User.Identity.Name);
            return View();
        }

        public ActionResult GenerateRPD(string plan, string discipline)
        {
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            return View();
        }

        [HttpPost]
        public ActionResult DownloadDocument(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var academicPlan = AcademicPlanService.GetAcademicPlan(structureDepartmentId, plan);
            var disciplinePlanItem = AcademicPlanService.GetDiscipline(structureDepartmentId, plan, discipline);
            ViewBag.Url = DocumentGeneratorService.CreateDocument(academicPlan, disciplinePlanItem, structureDepartmentId);
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            return View();
        }

        public ActionResult CopyRPD(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            //var previousDisciplines = AcademicPlanService.GetPreviousDisciplinesNames(structureDepartmentId, plan, discipline);
            //var nextDisciplines = AcademicPlanService.GetNextDisciplinesNames(structureDepartmentId, plan, discipline);
            //var copiedDiscipline = RpdParserService.Parse()
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            return View();
        }

        [HttpPost]
        public ActionResult CopyRPD(HttpPostedFileBase upload, string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            var previousDisciplines = AcademicPlanService.GetPreviousDisciplinesNames(structureDepartmentId, plan, discipline);
            var nextDisciplines = AcademicPlanService.GetNextDisciplinesNames(structureDepartmentId, plan, discipline);
            var copiedDiscipline = RpdParserService.Parse(upload.InputStream, previousDisciplines, nextDisciplines);
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            TempData["CopiedDiscipline"] = copiedDiscipline;
            return View(copiedDiscipline);
        }

        [HttpPost]
        public ActionResult SaveCopy(string discipline, string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plan = plan;
            ViewBag.Discipline = discipline;
            var copiedDiscipline = TempData["CopiedDiscipline"] as PlanItemDTO;
            var planItem = AcademicPlanService.GetDiscipline(structureDepartmentId, plan, discipline);
            var disciplineDto = copiedDiscipline.Knowledges[0] as DisciplineDTO;
            disciplineDto.PreviousDisciplines = disciplineDto.PreviousDisciplines
                .Select(dis => AcademicPlanService.GetDiscipline(structureDepartmentId, plan, dis.Name).Knowledges[0] as DisciplineDTO)
                .ToList();
            disciplineDto.NextDisciplines = disciplineDto.NextDisciplines
                .Select(dis => AcademicPlanService.GetDiscipline(structureDepartmentId, plan, dis.Name).Knowledges[0] as DisciplineDTO)
                .ToList();
            (planItem.Knowledges[0] as DisciplineDTO).Goals = disciplineDto.Goals;
            (planItem.Knowledges[0] as DisciplineDTO).Tasks = disciplineDto.Tasks;
            (planItem.Knowledges[0] as DisciplineDTO).PreviousDisciplines = disciplineDto.PreviousDisciplines;
            (planItem.Knowledges[0] as DisciplineDTO).NextDisciplines = disciplineDto.NextDisciplines;
            (planItem.Knowledges[0] as DisciplineDTO).Literature = disciplineDto.Literature;
            planItem.Knowledges[0].Results.Clear();
            planItem.Knowledges[0].Results.AddRange(disciplineDto.Results);
            DisciplineService.SavePlanItem(planItem);
            return View("CopyRPD");
        }
        #endregion

    }
}