﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using EducationalProgramGenerator.WEB.Models;
using EducationalProgramGenerator.BLL.Interfaces;
using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Infrastructure;
using System.Security.Claims;

namespace EducationalProgramGenerator.WEB.Controllers
{
    [Authorize]
    public class ManageController : BaseController
    {
        public ManageController()
        {
        }

        private IJobRoleService JobRoleService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IJobRoleService>();
            }
        }

        private IRoleService RoleService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IRoleService>();
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message, string selectedUserId = null)
        {
            ViewBag.StatusMessage =
            message == ManageMessageId.ChangePasswordSuccess ? "Ваш пароль изменен."
            : message == ManageMessageId.SetPasswordSuccess ? "Пароль задан."
            : message == ManageMessageId.Error ? "Произошла ошибка."
            : message == ManageMessageId.RemoveRoleSuccess ? "Роль удалена."
            : message == ManageMessageId.AddRoleSuccess ? "Роль добавлена."
            : message == ManageMessageId.ChangeStructureDepartmentSuccess ? "Структурное подразделение изменено."
            : message == ManageMessageId.ChangeJobRoleSuccess ? "Должность изменена."
            : message == ManageMessageId.ChangeFullNameSuccess ? "ФИО изменено."
            : message == ManageMessageId.ChangeEmailSuccess ? "Email изменен."
            : "";

            string userId = selectedUserId != null ? Decode(selectedUserId) : User.Identity.GetUserId();

            var user = await UserService.FindByIdAsync(userId);
            var structureDepartment = StructureDepartmentService.Get(user.StructureDepartmentId);
            var jobRole = JobRoleService.Get(user.JobRoleId);
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(user),
                StructureDepartment = structureDepartment,
                JobRole = jobRole,
                Roles = await UserService.GetRolesAsync(userId),
                UserID = userId,
                UserName = user.UserName,
                FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName),
                Email = user.Email
            };
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public ActionResult ManageUsers()
        {
            ViewBag.Users = UserService.GetAll();
            return View();
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> RemoveUserRole(string selectedUserId, string roleName)
        {
            var result = await UserService.RemoveFromRoleAsync(Decode(selectedUserId), roleName);
            ManageMessageId? message = null;
            if (result.Succeeded)
                message = ManageMessageId.RemoveRoleSuccess;
            return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> AddRole(string selectedUserId)
        {
            string userId = Decode(selectedUserId);
            var user = await UserService.FindByIdAsync(userId);
            var allRoles = RoleService.GetAll();
            var userRoles = await UserService.GetRolesAsync(userId);
            ViewBag.Roles = allRoles.Except(userRoles);
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            ViewBag.UserID = userId;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> AddRole(string selectedUserId, string roleName)
        {
            if (roleName != "")
            {
                var result = await UserService.AddToRoleAsync(Decode(selectedUserId), roleName);
                ManageMessageId? message = null;
                if (result.Succeeded)
                    message = ManageMessageId.AddRoleSuccess;
                return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
            }
            else
            {
                return View("Index", new { selectedUserId = selectedUserId });
            }
        }

        [Authorize(Roles = "admin, user")]
        public ActionResult ChangeStructureDepartment(string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var user = UserService.FindById(userId);
            ViewBag.StructureDepartments = StructureDepartmentService.GetAll();
            ViewBag.UserID = userId;
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin, user")]
        public ActionResult ChangeStructureDepartment(int structureDepartmentId, string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var result = UserService.ChangeStructureDepartment(userId, structureDepartmentId);
            ManageMessageId? message = null;
            if (result.Succeeded)
                message = ManageMessageId.ChangeStructureDepartmentSuccess;
            return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
        }

        [Authorize(Roles = "admin, user")]
        public ActionResult ChangeJobRole(string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var user = UserService.FindById(userId);
            ViewBag.JobRoles = JobRoleService.GetAll();
            ViewBag.UserID = userId;
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin, user")]
        public ActionResult ChangeJobRole(int jobRoleId, string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var result = UserService.ChangeJobRole(userId, jobRoleId);
            ManageMessageId? message = null;
            if (result.Succeeded)
                message = ManageMessageId.ChangeJobRoleSuccess;
            return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
        }

        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> ChangeFullName(string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var user = await UserService.FindByIdAsync(userId);
            ViewBag.UserID = userId;
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> ChangeFullName(ChangeFullNameViewModel model, string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var result = await UserService.ChangeFullNameAsync(userId, model.FirstName, model.SecondName, model.LastName);
            ManageMessageId? message = null;
            if (result.Succeeded)
                message = ManageMessageId.ChangeFullNameSuccess;
            return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
        }

        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> ChangeEmail(string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var user = await UserService.FindByIdAsync(userId);
            ViewBag.UserID = userId;
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin, user")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmail(string email, string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var result = await UserService.ChangeEmailAsync(userId, email);
            ManageMessageId? message = null;
            if (result.Succeeded)
                message = ManageMessageId.ChangeEmailSuccess;
            return RedirectToAction("Index", new { Message = message, selectedUserId = selectedUserId });
        }

        [Authorize(Roles = "admin, user")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                Tuple<OperationDetails, ClaimsIdentity> result = 
                    await UserService.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                if (result.Item1.Succeeded)
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, result.Item2);
                    return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                ModelState.AddModelError(result.Item1.Property, result.Item1.Message);
            }
            return View(model);
        }

        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> SetPassword(string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            var user = await UserService.FindByIdAsync(userId);
            ViewBag.UserID = userId;
            ViewBag.FullName = string.Format("{0} {1} {2}", user.LastName, user.FirstName, user.SecondName);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin, user")]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model, string selectedUserId = null)
        {
            string userId = GetUserIdDependingOnCurrentUserRole(selectedUserId);
            if (ModelState.IsValid)
            {
                Tuple<OperationDetails, ClaimsIdentity> result = 
                    await UserService.SetPasswordAsync(userId, model.NewPassword);
                if (result.Item1.Succeeded)
                {
                    if (userId == User.Identity.GetUserId())
                    {
                        AuthenticationManager.SignOut();
                        AuthenticationManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, result.Item2);
                        return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess, selectedUserId = selectedUserId });
                }
                ModelState.AddModelError(result.Item1.Property, result.Item1.Message);
            }
            ViewBag.UserID = userId;
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                RoleService.Dispose();
                JobRoleService.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Вспомогательные приложения
        // Используется для защиты от XSRF-атак при добавлении внешних имен входа
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private bool HasPassword(UserDTO user)
        {
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error,
            RemoveRoleSuccess,
            AddRoleSuccess,
            ChangeStructureDepartmentSuccess,
            ChangeJobRoleSuccess,
            ChangeFullNameSuccess,
            ChangeEmailSuccess
        }
        public static string Encode(string encodeMe)
        {
            byte[] encoded = System.Text.Encoding.UTF8.GetBytes(encodeMe);
            return Convert.ToBase64String(encoded);
        }

        public static string Decode(string decodeMe)
        {
            byte[] encoded = Convert.FromBase64String(decodeMe);
            return System.Text.Encoding.UTF8.GetString(encoded);
        }

        private string GetUserIdDependingOnCurrentUserRole(string selectedUserId)
        {
            return selectedUserId != null && UserService.IsInRole(User.Identity.GetUserId(), "admin")
                ? Decode(selectedUserId)
                : User.Identity.GetUserId();
        }
        #endregion
    }
}