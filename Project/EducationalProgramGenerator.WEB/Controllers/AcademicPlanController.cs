﻿using EducationalProgramGenerator.BLL.DTO;
using EducationalProgramGenerator.BLL.Interfaces;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Linq;

namespace EducationalProgramGenerator.WEB.Controllers
{
    [Authorize(Roles = "superuser, admin")]
    public class AcademicPlanController : BaseController
    {
        private IAcademicPlanService AcademicPlanService
        {
            get
            {
                return HttpContext.GetOwinContext().Get<IAcademicPlanService>();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var userFromAuthCookie = System.Threading.Thread.CurrentPrincipal;

            var user = UserService.FindByUserName(userFromAuthCookie.Identity.Name);
            if (user.StructureDepartmentId == 0)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Message",
                    message = "Сначала установите структурное подразделение"
                }));
            }
        }

        public ActionResult Index()
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.Plans = AcademicPlanService.GetAcademicPlansNames(structureDepartmentId);
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                var user = UserService.FindByUserName(User.Identity.Name);
                XDocument document = XDocument.Load(upload.InputStream);
                AcademicPlanService.SaveAcademicPlan(document, user.StructureDepartmentId);
            }
            return RedirectToAction("Index");
        }

        public ActionResult GetAcademicPlans()
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.AcademicPlans = AcademicPlanService.GetAcademicPlans(structureDepartmentId);
            return View();
        }

        public ActionResult EditAcademicPlan(string plan)
        {
            var structureDepartmentId = UserService.FindByUserName(User.Identity.Name).StructureDepartmentId;
            ViewBag.AcademicPlan = AcademicPlanService.GetAcademicPlan(structureDepartmentId, plan);
            return View();
        }
    }
}