﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EducationalProgramGenerator.WEB.DTOValidator
{
    public static class DTOValidator
    {
        public static List<ValidationResult> TryValidateObject(object dto)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(dto);
            Validator.TryValidateObject(dto, context, results, true);
            return results;
        }
    }
}