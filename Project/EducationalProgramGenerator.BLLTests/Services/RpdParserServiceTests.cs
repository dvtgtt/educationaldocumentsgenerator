﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EducationalProgramGenerator.BLL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EducationalProgramGenerator.BLL.DTO;

namespace EducationalProgramGenerator.BLL.Services.Tests
{
    [TestClass()]
    public class RpdParserServiceTests
    {
        private RpdParserService parser;
        private List<string> previousDiscipline;
        private List<string> nextDiscipline;

        [TestInitialize]
        public void SetupContext()
        {
            parser = new RpdParserService();
            previousDiscipline = new List<string> {"Иностранный язык", "Философия", "История"};
            nextDiscipline = new List<string> {"Электронный бизнес", "Управление проектами"};
        }

        [TestMethod()]
        public void ParseDoc()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
                parser.Parse(file, previousDiscipline, nextDiscipline);
        }

        [TestMethod()]
        public void ParseDocx()
        {
            var docFile = @"..\..\Services\11_РПД _Программирование.docx";
            using (var file = File.Open(docFile, FileMode.Open))
                parser.Parse(file, previousDiscipline, nextDiscipline);
        }

        [TestMethod()]
        public void ParseRtf()
        {
            var docFile = @"..\..\Services\RPD.rtf";
            using (var file = File.Open(docFile, FileMode.Open))
                parser.Parse(file, previousDiscipline, nextDiscipline);
        }

        [TestMethod()]
        public void ExtractGoalsAndTasks()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var discipline = (DisciplineDTO) planItem.Knowledges.FirstOrDefault();
                Assert.IsTrue(discipline.Goals != "");
                Assert.IsTrue(discipline.Tasks != "");
            }
        }

        [TestMethod()]
        public void ExtractCompetencies()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var competencyCodes = planItem.Knowledges.FirstOrDefault().Results
                    .Where(lo => lo is CompetencyDTO)
                    .Cast<CompetencyDTO>()
                    .Select(c => c.Code)
                    .ToList();

                Assert.IsTrue(competencyCodes.Contains("ОК-3"));
            }
        }

        [TestMethod()]
        public void ExtractPreviousDiscipline()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var discipline = (DisciplineDTO) planItem.Knowledges.FirstOrDefault();
                Assert.AreEqual(discipline.PreviousDisciplines.Count, previousDiscipline.Count);
            }
        }

        [TestMethod()]
        public void ExtractNextDiscipline()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var discipline = (DisciplineDTO) planItem.Knowledges.FirstOrDefault();
                Assert.AreEqual(discipline.NextDisciplines.Count, nextDiscipline.Count);
            }
        }

        [TestMethod()]
        public void ExtractMainLiterature()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var discipline = (DisciplineDTO) planItem.Knowledges.FirstOrDefault();
                var mainLiteratureCount = discipline.Literature
                    .Where(lit => lit.Section == "Основная литература")
                    .Count();
                Assert.AreEqual(mainLiteratureCount, 3);
            }
        }

        [TestMethod()]
        public void ExtractAdditionalLiterature()
        {
            var docFile = @"..\..\Services\4_РПД Экономика.doc";
            using (var file = File.Open(docFile, FileMode.Open))
            {
                var planItem = parser.Parse(file, previousDiscipline, nextDiscipline);
                var discipline = (DisciplineDTO)planItem.Knowledges.FirstOrDefault();
                var additionalLiteratureCount = discipline.Literature
                    .Where(lit => lit.Section == "Дополнительная литература")
                    .Count();
                Assert.AreEqual(additionalLiteratureCount, 11);
            }
        }
    }
}